# Inngangur

Í þessum hluta verður farið yfir nokkur hagnýt atriði sem gott er að skima yfir í upphafi. Einnig getur verið gagnlegt að koma aftur að þessum kafla eftir að hafa öðlast dálitla reynslu en þá gæti reynst auðveldara að setja þessa hluti í samhengi.

## Hvað er breyta?

Í Python er allt - heiltölur, kommutölur, strengir, stærri gagnatög eins og tætitöflur, föll og annað - útfært sem hlutir (e. objects).  
Við getum hugsað okkur hlut sem ílát sem inniheldur gögn. Hluturinn er af einhverju tagi, til dæmis heiltala, og hvað við getum gert við hlutinn ræðst af tagi hans.  
Langflest forritunarmál leyfa okkur skilgreina breytur (e. variables). Breytur eru nöfn sem vísa á gildi í minni tölvunnar og auðvelda okkur að nota þessi gildi í forritunum okkar. Í Python notum við jafnaðarmerki (`=`) til að veita breytu tiltekið gildi.  

```python
>>> a = 7
>>> print(a)
7
```

Varðandi breytur er rétt að taka fram að breytur eru bara nöfn. Gildisveiting afritar ekki gildi heldur tengir hún einungis nafn við ákveðinn hlut sem inniheldur gögn. Nafnið sjálft er því frekar vísun á hlut frekar en hluturinn sjálfur. Ef við hugsum okkur jólapakka, þá gætum við hugsað okkur að hluturinn sé gjafapappírinn, gögnin séu gjöfin sjálf og breytan sé merkimiðinn.

### Mynd af jólapakka

Til að kanna tag tiltekinnar breytu getum við notað fallið `type()`:

```python
>>> nafn = 'Blær'
>>> type(nafn)
<class 'str'>
```

## Python skelin

### Hvar finn ég Python skelina?

Í sumum kóðadæmum eru línur sem byrja á þremur hægri fleygum (`>>>`) og tákna kvaðningu (e. prompt) Python-skeljarinnar. Þau gefa einungis til kynna að viðkomandi dæmi sé slegið inn í Python-skelina og eru fleygarnir því <u>**aldrei**</u> eitthvað sem notandinn slær inn.  
Á ensku er hún gjarnan kölluð *The Python interactive console* eða *The Python shell* en hér verður hún bara kölluð einu nafni; *Python skelin*.  
Python skelin gerir okkur kleift að prófa ákveðna eiginleika forritunarmálsins án þess að þurfa að búa til skrá og keyra hana. Þar höfum við aðgang að öllum innbyggðum föllum Python og forritseiningum sem eru aðgengileg á viðkomandi tölvu.  

Python skelin býður einnig upp á notendavæna eiginleika:  

- Skipanasögu (e. command history), sem gerir okkur mögulegt að fletta til baka og leita að gömlum skipunum
- Sjálf-fyllingu (e. auto-completion)
- Að afrita og líma kóða, bæði inn í skelina og úr henni  

Þegar við sláum eitthvað inn í Python skelina sem hefur gildi þá endurtekur skelin það sem við skrifuðum. Tökum sem dæmi hvað gerist ef við sláum bara inn töluna 5.

```Python
>>> 5
5
```

Þessi eiginleiki gerir okkur kleift að spara smá tíma þegar við viljum prófa að reikna eitthvað eða athuga hvað ein skipun gerir. Þegar við erum að skrifa forrit þá þurfum við að nota innbyggða fallið *print()* til þess að prenta út gildi. Takið eftir að við getum líka notað print í skelinni og fáum sömu niðurstöðu og áður:

```Python
>>> print(5)
5
```

Við getum líka notað skelina til þess að reikna:

```Python
>>> 5 + 5
10
```

Annað sem getur verið gagnlegt við Python skelina er að við getum skilgreint breytur og gefið þeim gildi. Þannig getum við prófað kóðabúta án þess að keyra heilu forritin.

```Python
>>> a = 5
>>> b = 10
>>> c = a + b
>>> c
15
```

Þó svo að skipanasagan sé geymd, þá eru breytur og annað slíkt einungis geymdar í vinnsluminni tölvunnar á meðan við notum Python skelina. Því er ekki nauðsynlegt að hafa áhyggjur af geymsluplássi tölvunnar. Um leið og við lokum Python skelinni þá gleymast allar breytur.

## En ef ég vil geyma forritið?

#### Aðeins um ritla og Pythonskrár (.py)

Þó svo að Python-skelin sé vissulega gagnlegt verkfæri þá hentar hún ekki til að skrifa lengri forrit. Þar að auki geymir hún alla hluti og kóða í vinnsluminni og glatar þeim því þegar við förum út úr henni.  
Þess vegna skulum við núna skoða hvernig við getum geymt kóðann okkar.  
Við getum skrifað Python-kóða í nánast hvaða ritli sem er og vistað með nafnaukanum `.py`. Til eru fjölmargir ritlar sem eru heppilegir fyrir forritun en athugið að ekki er heppilegt að nota svokölluð ritvinnsluforrit eins og til dæmis *MS Word*, þau eru ekki hugsuð fyrir ritun forritunarkóða.  

Margir ritlar bjóða stuðning við algeng forritunarmál með auðkenningu málskipanar (e. syntax highlighting), sjálfvirks inndráttar og fleira. Fyrir byrjendur er ekki endilega gott að velja of flókinn ritil og betra að einbeita sér að kóðanum sjálfum.

Ef við notumst til dæmis við hinn formlega Python ritil, *IDLE*, getum við vistað skránna og keyrt forritið að því loknu úr ritlinum. Skoðum hvernig það ferli gæti litið út í IDLE:

Þegar við opnum IDLE tekur á móti okkur gluggi með Python skelinni. Til að skrifa kóða til að vista smellum við á *File* í verkfærastiku gluggans og veljum *New File*.

#### Mynd - ritill1.png

Þá ætti að opnast nýr gluggi sem er auður ritill.

#### Mynd - ritill2.png

Til að geta keyrt kóða úr ritlinum þurfum við að vista hann sem skrá. Mikilvægt er að hafa í huga að vista skrár áður en þær eru keyrðar, annars taka síðustu breytingar ekki gildi.

#### Mynd - ritill3.png

Þegar við höfum vistað forritið okkar getum við keyrt það með því að smella á *Run* í verkfærastikunni og síðan á *Run Module*. Einnig er hægt að smella á *F5* hnappinn á lyklaborðinu til að keyra forritið.

#### Mynd - ritill4.png

Frálag forritsins birtist svo í glugga Python skeljarinnar.

#### Mynd - ritill5.png

xxx

## Málskipan (e. syntax)

Málskipan forritunarmála eru reglur um það hvaða röðun og samsetning tákna teljast réttar fyrir viðkomandi forritunarmál. Málskipan hefur því áhrif á það hvernig forrit eru skrifuð og túlkuð (bæði af inningarkerfi og manneskjum).

### Málskipan Python != flest önnur forritunarmál

Hér verða taldir upp nokkrir þættir varðandi málskipan Python sem greinir það frá mörgum öðrum forritunarmálum.  

* Setningar þurfa ekki að enda á tákni (eins og t.d. `;`) heldur enda þær með nýrri línu.

* Eigi bálkur að tilheyra setningu er það tilgreint með tvípunkti (`:`).

* Bálkar setninga eru skilgreindir með inndrætti (4 bil) en ekki slaufusvigum (`{}`).

* Ekki þarf að tilgreina tag þegar breytur eru skilgreindar.

* Oft eru notuð orð í stað tákna (dæmi: `and` í stað `&&`).

Skoðum stutt dæmi þar sem þessir þættir koma fyrir:

```python
y = True
x = 1
while x < 20 and y == True :
    print("Spennandi skilaboð")
    x += 1
```

Markmið Python með þessum reglum er að kóðinn verði sem snyrtilegastur og auðlesinn. 

Mörg forritunarmál nota tákn eins og slaufusviga (`{ }`) eða lykilorð eins og `begin` og `end` til að skorða af kóðahluta. Í þeim forritunarmálum þykir það yfirleitt góður siður að halda sig við sama inndrátt af festu og stundum mæla höfundar með ákveðnum siðum varðandi hluti eins og inndrátt og línuskil. Allt er þetta gert með það að markmiði að kóði verði læsilegri fyrir sem flesta.  
Við hönnun Python ákvað Guido Van Rossum (höfundur Python) að inndráttur ætti að vera nóg til að skilgreina uppbyggingu forrita og forðaðist því sem mest notkun sviga og slaufusviga. Þessi ákvörðun markaði Python ákveðna sérstöðu og þetta er nokkuð sem byrjendur taka oft eftir og þeim sem hafa reynslu af öðrum forritunarmálum finnst þetta stundum skrítið.  

### Frátekin orð

Í Python eru nokkur frátekin orð sem **ekki** má nota sem nöfn fyrir breytur, föll eða nein kennimerki (e. identifiers). Þessi fráteknu orð eru öll rituð með lágstöfum og mega auðvitað alveg koma fyrir sem hluti af öðru orði (til dæmis *not* í *notandi*).

| ÞESSI   | ERU      | FRÁTEKIN |
| ------- | -------- | -------- |
| and     | as       | assert   |
| async   | await    | break    |
| class   | continue | def      |
| del     | elif     | else     |
| except  | exec     | False    |
| finally | for      | from     |
| global  | if       | import   |
| in      | is       | lambda   |
| None    | nonlocal | not      |
| or      | pass     | print    |
| raise   | return   | True     |
| try     | while    | with     |
| yield   |          |          |

### Athugasemdir - fyrir aftan \#

Athugasemdir (e. comments) innihalda texta í forritum sem þýðendur (e. compilers) og túlkar (e.interpreters) hunsa. Ólík forritunarmál nota ólík tákn fyrir athugasemdir en Python notar millumerki **\#**.

Ef lína byrjar á millumerki sleppir Python túlkurinn þeirri línu en ef millumerki kemur fyrir síðar í línu, þá les túlkurinn línuna að millumerkinu og fer svo í næstu línu.

```python
# Þetta er athugasemd
>>> print('Mikilvæg skilaboð') # Þetta er líka athugasemd
Mikilvæg skilaboð
```

Millumerkið er fyrir staka línu en við ákveðnar aðstæður gætum við viljað láta athugasemd spanna fleiri en eina línu og án þess að hver þeirra byrji á millumerki. Til að gera það getum við notað þrjú högg í röð (`'''`) til að hefja athugasemd og svo þrjú högg til að enda hana. Athugasemdir sem þessi geta til dæmis reynst gagnlegar við upphaf kóða til að útskýra virkni hans, bæði til áminningar fyrir þann sem skrifar en einnig fyrir aðra sem eiga mögulega eftir að lesa kóðann.

```python
'''
Þetta er athugasem
sem spannar
margar línur.
'''
```

Þó svo að athugasemdir fylgi ekki öllum dæmum, þá getur verið mjög gagnlegt að skrifa sínar eigin athugasemdir. Það á sérstaklega við ef efnið er flókið eða tengist kannski einhverju sem við notum sjaldan. Það getur komið sér vel að eiga til kóðadæmi með góðum athugasemdum, til dæmis þar sem notuð er ákveðin forritseining.

Góður forritari skrifar góðar athugasemdir.

## Villuboð - ekki taka þeim illa!

Tilgangur villuboðanna er ekki að láta okkur líða illa. Þau eru til að hjálpa okkur að finna út úr villum sem upp koma.
Það munu koma upp villur, það gerist hjá **öllum**, sama hvort við erum byrjendur eða með áratuga reynslu!

Algengustu villur eru einfaldar innsláttarvillur og því getur verið mjög gagnlegt að lesa yfir villuboðin því þau benda okkur gjarnan á staðsetningu villunnar.

Margir tala um að *aflúsa* kóða þegar villur eru lagaðar en það hugtak kemur frá enska orðinu *debugging*. Hugtakið debugging er gjarnan kennt við Grace Hopper en hún notaði það hugtak þegar hún, ásamt samstarfsfólki hennar, fjarlægði pöddur úr tölvum á fimmta áratug síðustu aldar.

### Mynd af Grace Hopper

## Ekki bara lesa, prófa þú líka!

Þar sem kóðadæmi koma fyrir er mikilvægt að allir prófi sjálfir. Enn betra er að prófa líka að gera breytingar á dæmum til að átta sig betur á virkninni og hvaða hlutir gera hvað.

## Endurtekning

Þegar við höfum leyst ákveðið verkefni getur verið mjög gagnlegt að leysa það aftur frá grunni og þá með því að nota leiðbeiningar sem minnst. Þegar við getum leyst verkefni án þess að kíkja á lausnina ...












