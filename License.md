Nema annað sé tekið fram er allt efni undir leyfi **Creative Commons Attribution-ShareAlike 4.0**.  
![Táknmynd CC BY-SA 4.0](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)  
<br>
**Eignun** - Þú verður að veita viðeigandi viðurkenningu, sjá fyrir krækju að leyfinu ásamt því að tilgreina ef einhverjar breytingar voru gerðar á efninu. Þér er heimilt að gera það á skynsamlegan máta en þó ekki á nokkurn hátt sem gæti gefið til kynna að leyfisveitandi ljái þér, eða notkunn þinni, sérstaklega fylgi sitt.

**Deilist áfram (eins)** - Ef þú endurblandar, umbreytir eða byggir á efninu, verður þú að dreifa þínu framlagi undir sama leyfi og hið upprunalega.

<br>
Þetta þýðir, svo framarlega sem að fyrrgreindum skilmálum sé fylgt, að öllum er frjálst að:  

    * Deila - afrita og endurútgefa efnið á hvaða miðli og á með hvaða sniði sem er
    * Aðlaga - endurblanda, breyta og/eða byggja ofan á efnið í hvaða tilgangi sem er, jafnvel

Leyfisveitandi getur ekki afnumið þessi réttindi svo framarlega sem að leyfi er fylgt.
 
[Sjá leyfið í heild sinni hér.](https://creativecommons.org/licenses/by-sa/4.0/legalcode)


<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

