# forritseiningar

Kafli um forritseininagar og Py std lib

## Hvernig flytjum við inn forritseiningar?

Þrjár leiðir:

 * Til að flytja inn heila forritseiningu:
    + `import <forritseining>`
 * Til að flytja inn hlut úr forritseiningu:
    + `from <forritseining> import <hlutur>`
 * Til að flytja inn allt úr forritseiningu sem staka hluti:
    + `from <forritseining> import *`



### Getum flutt inn hluti úr okkar eigin Python-skrám
