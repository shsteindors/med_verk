Python hefur að geyma nokkur innbyggð föll \(e. built-in functions\) sem er hægt að nota án þess að setja þau sérstaklega upp. Hér ætlum við að skoða nokkur þeirra.  

### abs

Fallið 'abs' skilar algildi tölu. Algildi tölu er tala sem er ekki neikvæð og til dæmis getum við tekið algildið af 10 og fáum 10. Ef við tökum algildið af -10 þá fáum við líka 10.  

```python
>>> print(abs(10))
10
>>> print(abs(-10))
10
```



### bool

Fallið 'bool' skilar annað hvort satt \(e. True\) eða ósatt \(e. False\) og byggir skilagildið á þeim inntaksbreytum sem eru notaðar þegar kallað er á fallið. Ef kallað er á fallið með 0 þá skilar fallið ósönnu á meðan allar aðrar tölur skila sönnu.  

```python
>>> print(bool(0))
False
>>> print(bool(1))
True
>>> print(bool(1123.23))
True
>>> print(bool(-500))
True
```



Ef önnur gildi en tölur eru notuð til þess að kalla á 'bool' þá skilar None ósatt á meðan öll önnur gildi skilar sönnu :  

```python
>>> print(bool(None))
False
>>> print(bool(’a’))
True
```



### cmp

Fallið 'cmp' ber saman tvö gildi og skilar neikvæðri tölu ef fyrra gildið er minna en seinna gildið; skilar 0 ef fyrra gildi er jafnt seinna gildinu; og skilar jákvæðri tölu ef fyrra gildið er stærra en seinna gildið. Til dæmis þá er 1 minna en 2:  

```python
>>> print(cmp(1,2))
-1
```

Og 2 er jafnt og 2:  

```python
>>> print(cmp(2,2))
0
```

En 2 er stærra en 1:  

```python
>>> print(cmp(2,1))
1
```

Fallið ber ekki eingöngu saman tölur heldur er hægt að nota aðrar tegundir af gildum, svo sem strengi :  

```python
>>> print(cmp(’a’,’b’))
-1
>>> print(cmp(’a’,’a’))
0
>>> print(cmp(’b’,’a’))
1
```

Hins vegar er ekki víst að samanburður strengja skili því sem maður á von á...  

```python
>>> print(cmp(’a’,’A’))
1
>>> print(cmp(’A’,’a’))
-1
```

Lítið 'a' er nefnilega "stærra" en stórt 'A'.  En það segir sig sjálft að...  

```python
>>> print(cmp(’aaa’,’aaaa’))
-1
>>> print(cmp(’aaaa’,’aaa’))
1
```

...3 'a' \(aaa\) eru minna en 4 'a' \(aaaa\).

### dir

Fallið 'dir' skilar lista af upplýsingum um gildi. Við getum notað 'dir' á strengi, tölur, föll, forritseiningar, hluti og klasa. Ef við notum 'dir' á töluna 1 þá fáum við :  

```python
>>> dir(1)
[’__abs__’, ’__add__’, ’__and__’, ’__class__’, ’__cmp__’, ’__coerce__’, ’__delattr__’,
’__div__’, ’__divmod__’, ’__doc__’, ’__float__’, ’__floordiv__’, ’__getattribute__’,
’__getnewargs__’, ’__hash__’, ’__hex__’, ’__index__’, ’__init__’, ’__int__’, ’__invert__’,
’__long__’, ’__lshift__’, ’__mod__’, ’__mul__’, ’__neg__’, ’__new__’, ’__nonzero__’,
’__oct__’, ’__or__’, ’__pos__’, ’__pow__’, ’__radd__’, ’__rand__’, ’__rdiv__’,
’__rdivmod__’, ’__reduce__’, ’__reduce_ex__’, ’__repr__’, ’__rfloordiv__’,
’__rlshift__’, ’__rmod__’, ’__rmul__’, ’__ror__’, ’__rpow__’, ’__rrshift__’, ’__rshift__’,
’__rsub__’, ’__rtruediv__’, ’__rxor__’, ’__setattr__’, ’__str__’, ’__sub__’, ’__truediv__’,
’__xor__’]

```

... sem er slatti af gildum.  

Ef við prófum að skoða strenginn ```'a'``` þá fáum við :  

```python
>>> dir(’a’)
[’__add__’, ’__class__’, ’__contains__’, ’__delattr__’, ’__doc__’, ’__eq__’, ’__ge__’,
’__getattribute__’, ’__getitem__’, ’__getnewargs__’, ’__getslice__’, ’__gt__’, ’__hash__’,
’__init__’, ’__le__’, ’__len__’, ’__lt__’, ’__mod__’, ’__mul__’, ’__ne__’, ’__new__’,
’__reduce__’, ’__reduce_ex__’, ’__repr__’, ’__rmod__’, ’__rmul__’, ’__setattr__’,
’__str__’, ’capitalize’, ’center’, ’count’, ’decode’, ’encode’, ’endswith’, ’expandtabs’,
’find’, ’index’, ’isalnum’, ’isalpha’, ’isdigit’, ’islower’, ’isspace’, ’istitle’,
’isupper’, ’join’, ’ljust’, ’lower’, ’lstrip’, ’partition’, ’replace’, ’rfind’, ’rindex’,
’rjust’, ’rpartition’, ’rsplit’, ’rstrip’, ’split’, ’splitlines’, ’startswith’, ’strip’,
’swapcase’, ’title’, ’translate’, ’upper’, ’zfill’]
```

Þarna sjáum við til dæmis fallið `capitalize` sem breytir fyrsta staf strengs í hástaf.  

```python
>>> print(’aaaaa’.capitalize())
Aaaaa
```

`dir()` er til dæmis gagnlegt þegar við viljum vita hvað við getum gert við ákveðnar breytur eða tög.

### eval

Fallið 'eval' tekur inn streng sem inntaksbreytu (e. parameter) og keyrir það líkt og um Python segð (e. expression) væri að ræða. Fallið er sambærilegt lykilorðinu 'exec' en virkar aðeins öðruvísi. Með 'exec' getum við búið til ör-útgáfur af Python forritum í strengjunum okkar, en 'eval' leyfir eingöngu einfaldar segðir líkt og :

```python
>>> eval(’10*5’)
50
```

### ﬁle

Fallið 'file' er hægt að nota til þess að opna skrá og skila skráarhlut sem inniheldur föll sem geta nálgast upplýsingarnar sem eru geymdar í skránni. Ítarlegri upplýsingar um 'file' er að finna í kafla 7.

### ﬂoat

Fallið 'float' breytir streng eða tölu í hlaupakommutölu (e. floating-point decimal). Hlaupakommutala er tala sem er með aukastafi (e. decimal places).

Til að mynda er 10 heil tala (e. integer) á meðan 10.0, 10.1 og 10.253 eru allt hlaupakommutölur. Við getum breytt streng í kommutölu með því að gera:

```python
>>> float(’12’)
12.0
```

Við getum einnig sett kommutákn í strenginn þegar við köllum á 'float':

```python
>>> float(’123.456789’)
123.456789
```

Heiltölu er hægt að breyta í kommutölu með því að gera:

```python
>>> float(200)
200.0
```

Ef við reynum að breyta kommutölu í kommutölu þá fáum við að sjálfsögðu kommutölu:

```python
>>> float(100.123)
100.123
```

Ef við innum 'float' án inntaksbreyta (með ekkert á milli sviganna) þá skilar fallið ```0.0```.

### int

Fallið breytir streng eða tölu í heiltölu \(e. integer\). Sem dæmi:

```python
>>> int(123.456)
123
>>> int(’123’)
123
```

Fallið `int()` virkar aðeins öðruvísi en 'float' því ef við reynum að breyta streng, sem inniheldur kommutölu í heiltölu, þá fáum við villu :

```python
>>> int(’123.456’)
Traceback (most recent call last):
File "<stdin>", line 1, in <module>
ValueError: invalid literal for int() with base 10: ’123.456’
```

Ef við reynum að kalla á 'int' með engum inntaksbreytum þá skilar fallið gildinu ```0```.

### len

Fallið `len()` skilar lengd hlutar. Ef hluturinn er strengur þá skilar fallið fjölda stafa í strengnum:  

```python
>>> len(’Þetta er tilraunastrengur’)
26
```

Fyrir lista eða línur (e. tuple) þá skilar fallið fjölda hluta:

```python
>>> listi = [ ’a’, ’b’, ’c’, ’d’ ]
>>> print(len(listi))
4
>>> lina = (1,2,3,4,5,6)
>>> print(len(lina))
6
```

Fallið skilar einnig fjölda hluta í fylki \(e. map\):

```python
>>> fylki = { ’a’ : 100, ’b’ : 200, ’c’ : 300 }
>>> print(len(fylki))
3
```

`len()` er gagnlegt þegar við vinnum með lykkjur og viljum telja fjölda hluta í lista. Við getum gert það á eftirfarandi hátt:

```python
>>> listi = [ ’a’, ’b’, ’c’, ’d’ ]
>>> for hluti in listi:
...     print(hluti)
```

Dæmið hér að ofan myndi prenta alla hlutina í listanum en hvað ef við viljum sýna lykla (e. index) hvers hlutar í listanum?  
Þá þurfum við að finna lengd listans og telja síðan í gegnum alla hlutina í listanum á eftirfarandi hátt:  





```python
>>> listi = [ ’a’, ’b’, ’c’, ’d’ ]
>>> lengd = len(listi)
>>> for x in range(0, lengd):
... print(’hlutur númer %s er %s’ % (x, listi[x]))
...
hlutur númer 0 er a
hlutur númer 1 er b
hlutur númer 2 er c
hlutur númer 3 er d
```

Við vistum lengd listans í breytunni `lengd` og notum síðan þá breytu til þess að búa til bilið sem lykkjan okkar vinnur á.



### max

Fallið `max()` skilar stærsta hlutnum í lista, línu eða streng:

```python
>>> listi = [ 5, 4, 10, 30, 22 ]
>>> print(max(listi))
30
```

Fallið virkar líka á hluti sem eru aðskildir með kommum eða bili:

```python
>>> s = ’a,b,d,h,g’
>>> print(max(s))
h
```

Það þarf því ekki að búa til lista, línur eða strengi. Fallið er líka hægt að nota beint á mörg gildi (eða breytur) í einu:

```python
>>> print(max(10, 300, 450, 50, 90))
450
```

### min

Fallið `min()` virkar á sama hátt og `max()` nema það skilar **minnsta** gildinu en ekki stærsta:

```python
>>> listi = [ 5, 4, 10, 30, 22 ]
>>> print(min(listi))
4
```

### range

Fallið `range()` er aðallega notað í for-lykkjur þegar við viljum "lykkja" í gegnum sama kóðann nokkrum sinnum. Fallið má nota með einni, tveimur eða þremur inntaksbreytum (e. arguments). Sú fyrsta stendur fyrir upphafsgildið, sú næsta fyrir endagildið (við þurfum bara að muna að Python byrjar að telja á núlli og endagildið miðast við það).

```python
>>> for x in range(0, 5):
...	    print(x)
0
1
2
3
4
```

```range()``` er í raun að skila runu af heiltölum sem lykkjan vinnur svo á.  
Við getum breytt ítraranum í lista (með ```list()``` fallinu) og með því að prenta listann getum við séð útkomuna:

```python
>>> print(list(range(0, 5)))
[0, 1, 2, 3, 4]
```

Við getum búið til lista af tölum sem er hægt að úthluta til annarra breyta og nota annars staðar í forritinu okkar:

```python
>>> listi_af_tolum = list(range(0, 30))
>>> print(listi_af_tolum)
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29]
```

Fallið getur einnig tekið inn þriðju inntaksbreytuna sem kallast þrep (e. step) (fyrstu tvær inntaksbreyturnar kallast upphaf (e. start) og endir (e. stop)). Ef við sleppum því að setja inn þrepabreytuna þá er hún sjálfkrafa ```1```.  
Hvað ætli gerist ef við setjum þrepabreytuna sem 2?

```python
>>> listi_af_tolum = list(range(0, 30, 2))
>>> print(listi_af_tolum)
[0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28]
```

Hvert gildi í listanum var 2 hærri en talan á undan. Við getum svo notað enn stærri þrep:

```python
>>> listi = list(range(0, 500, 50))
>>> print(listi)
[0, 50, 100, 150, 200, 250, 300, 350, 400, 450]
```

Þessi aðgerð býr til lista af tölum frá 0 til 500 (500 er ekki inni í bilinu) og þrepar upp í næstu tölu með 50.

### sum

Fallið 'sum' leggur saman hluti í lista og skilar heildartölunni:

```python
>>> listi = list(range(0, 500, 50))
>>> print(listi)
[0, 50, 100, 150, 200, 250, 300, 350, 400, 450]
>>> print(sum(listi))
2250
```
