# Kafli um innviði tölvunnar

Hér verður komið inn á innviði tölvu, sérstaklega minni og í mjög stuttu máli.

## Vinnsluminni (e. Random Access Memory -> RAM)
A computer system has two types of memory: short-term random access memory
(RAM) that is fast but limited (usually measured in gigabytes with access times
in nanoseconds) and long-term that is thousands of times slower but far more
vast (often measured in terabytes with access times in milliseconds). Additionally,
a computer has a few small brains or central processing units (CPUs) that execute
instructions, input and output mediums, and a network connection to support
the exchange of information between machines.


## Stærðir og mælikvarðar

Mass storage devices and memories are very large and thus measured with units that
have names different from those used in everyday life. While we use the colloquial word
grand to refer to $1,000, for amounts greater than $1,000 we use the names of the decimal
system, such as million. These are not universally used—in the United States, one thou‐
sand million is called billion; in Europe it is called milliard. There is, however, an agreed
upon nomenclature for powers of 10 so that one thousand is called kilo, one million is called Mega, and so on (see Table 2-1). 

Margir kannast eflaust við að umræður varðandi síma og aðrar tölvur þar sem rætt er um magn vinnsluminnis og geymslupláss. Vinnsluminni er gjarnan í gígabætum en geymslupláss er gjarnan mælt í gígabætum á símum og smærri tölvum en í terabætum á þeim stærri. Eftir því sem geymslumiðlar verða ódýrari geymir fólk meira af gögnum.  
Þar sem geymslubúnaður og minni geta verið mjög stór og því notum við gjarnan einingar sem gera umfang þess sem unnið er með þægilegra. Ekki kalla heldur allir sömu stærði sömu nöfnum og til dæmis köllum við þúsund milljónir (1,000,000,000) *milljarð* á meðan enskumælandi vestanhafs nota orðið *billion*. Fyrir okkur þýðir orðið *billjón* eða þúsund milljarðar (1,000,000,000,000).  
 

| Metrakerfi (sk.) | Þáttur         | Stærð                     | Tvíundakerfi (sk.) | Þáttur         | Stærð                     |
|-----------------------|----------------|---------------------------|-------------------------|----------------|---------------------------|
| *Kílóbæti (kB)*         | 10<sup>3</sup> | 1,000                     | *Kíbibæti (KiB)*          | 2<sup>10</sup> | 1,024                     |
| *Megabæti (MB)*         | 10<sup>6</sup> | 1,000,000                 | *Mebibæti (MiB)*          | 2<sup>20</sup> | 1,048,576                 |
| *Gígabæti (GB)*         | 10<sup>9</sup> | 1,000,000,000             | *Gíbibæti (GiB)*          | 2<sup>30</sup> | 1,073,741,824             |
| *Terabæti (TB)*         | 10<sup>12</sup> | 1,000,000,000,000         | *Tebibæti (TiB)*          | 2<sup>40</sup> | 1,099,511,627,776         |
| *Petabæti (PB)*         | 10<sup>15</sup> | 1,000,000,000,000,000     | *Pebibæti (PiB)*          | 2<sup>50</sup> | 1,125,899,906,842,624     |
| *Exabæti (EB)*          | 10<sup>18</sup> | 1,000,000,000,000,000,000 | *Exbibæti (EiB)*          | 2<sup>60</sup> | 1,152,921,504,606,846,976 |

### Athugasemd
Hey, af hverju eru allar skammstafanir með stóran staf nema Kíló?  



Note the lowercase in kilo, the uppercase in Mega,
and all that follow. This comes from the fact that the letter K is reserved, in the decimal
nomenclature, for the designation of the absolute temperature measure (degrees in
Kelvin).