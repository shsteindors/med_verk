# Að vinna með skrár

Það gætu komið upp þær aðstæður að við þurfum að geta lesið gögn sem eru geymd í skrám. Ef við erum til dæmis að gera *Hengimann* leikinn þá gæti verið þægilegt að lesa inn orðin úr skrá frekar en að búa þau öll til í forritinu sjálfu.

Í þessum kafla verður farið yfir nokkur atriði sem gera okkur kleift að vinna með textaskrár.

Til þess að geta lesið gögnin þá þurfum við að opna skránna en það gerum við með fallinu `open()`. Fallið skilar af sér breytu sem við getum notað til þess að lesa innihald skráarinnar og fallið tekur inn slóð skráarinnar ásamt inntaksgildið *mode*. *Mode* segir til um í hvaða ham skráin opnast, þ.e. hvað við viljum gera við skránna. Athugið að í dæmunum hér að neðan munum við vinna með skrár á sama stað og Python er staðsett.

```python
>>> with open('skra.txt','r') as skra:
	for i in skra:
		print(i)


Ég bið að heilsa!



Nú andar suðrið sæla vindum þýðum

á sjónum allar bárur smáar rísa

og flykkjast út að fögru landi Ísa-,

að fósturjarðar minnar strönd og hlíðum.



Ó, heilsið öllum heima í orðum blíðum

um haf og land í drottins ást og friði,

leiði þið bárur! bát að fiskimiði

blási þið vindar hlýtt á kinnum fríðum.



Söngvarinn ljúfi, fuglinn trúr sem fer

með fjaðrabliki háa vegarleysu

lágan dal, að kveða kvæðin þín,



heilsaðu einkum ef að fyrir ber

engill með húfu og grænan skúf í peysu;

þröstur minn góður! það er stúlkan mín.



höf.: Jónas Hallgrímsson
```

Í dæminu hér að ofan notuðum við haminn `'r'` en hann segir `open()` fallinu að við viljum eingöngu lesa skránna. Við getum því sagt að við höfum opnað skránna í lesham. Við notuðum síðan *for*-lykkju til þess að prenta innihald skráarinnar á skjáinn.

Við getum líka sótt eina línu í einu:

```python
>>> with open('skra.txt','r') as skra:
	skra.readline()


'Ég bið að heilsa!\n'
```
Þarna gerðist eitthvað skrítið. Þegar við prentuðum skránna áðan þá var fyrsta línan "Ég bið að heilsa!" en þegar kölluðum á eina línu án þess að nota `print()` fallið þá bættist við `\n`. Þetta tákn (`\n`) þýðir „næsta lína“ og er það sem við köllum falið tákn. Táknið verður alltaf til þegar við ýtum á *Enter*-takkann á lyklaborðinu en flestir ritlar fela þetta tákn. Skipunin `readline()` sýnir okkur táknið því við notuðum það í skelinni án þess að færa skilagildið undir breytu.

Skoðum næst alla möguleikana sem við höfum þegar við notum fallið `open()`.

| Tákn | Hamur (e. mode)                                            |
|------|------------------------------------------------------------|
|  'r' | Opna skrá í lesham                                         |
|  'w' | Opna skrá í skrifham                                       |
|  'x' | Búa til skrá ef hún er ekki til                            |
| 'a'  | Opna skrá, ef hún er til, til þess að skrifa aftast í hana |
| 'b'  | Tvíundarhamur                                              |
| 't'  | Textahamur (sjálfgefinn hamur)                             |
| '+'  | Opna skrá í les- og skrifham                               |

Skoðum nokkur dæmi um hvernig við getum notað fallið.

```python
>>> with open('skra.txt','w') as skra:
	skra.write('Þetta er tilraun 1\n')
	skra.write('Þetta er tilraun 2\n')


19
19
>>> with open('skra.txt','r') as skra:
  	for i in skra:
  		print(i)


Þetta er tilraun 1

Þetta er tilraun 2

>>>
```

Þegar við notuðum `'w'` þá sjáum við að Python skrifaði yfir allt í skránni með nýja textanum. Ef við viljum eingöngu bæta við skránna þá notum við `'a'`:

```python
>>> with open('skra.txt','a') as skra:
	skra.write('Skrifaðu þetta neðst í skránna')
	skra.write('Æ,æ, ég gleymdi að setja inn nýja línu')


30
38
>>> with open('skra.txt','r') as skra:
  	for i in skra:
  		print(i)


Þetta er tilraun 1

Þetta er tilraun 2

Skrifaðu þetta neðst í skránnaÆ,æ, ég gleymdi að setja inn nýja línu
```

Nú sjáum við að við skrifuðum ekki yfir allt sem var í skránni en þar sem við gleymdum að setja inn línuskil í línurnar þá komu nýju línurnar inn í sömu línu.

Ef við viljum búa til nýja skrá þá getum við notað `'x'`-haminn:

```python
>>> with open('skra2.txt','x') as skra:
	skra.write('þetta er ný skrá')


16
>>> with open('skra2.txt','r') as skra:
	for i in skra:
		print(i)


þetta er ný skrá
```

Við verðum að passa að það má ekki vera til skrá með sama heiti á sama stað og við ætlum að búa til nýja skrá:

```python
>>> with open('skra2.txt','x') as skra:
	skra.write('Þessi heitir sama nafni og önnur skrá í sömu möppu')


Traceback (most recent call last):
  File "<pyshell#8>", line 1, in <module>
    with open('skra2.txt','x') as skra:
FileExistsError: [Errno 17] File exists: 'skra2.txt'
```
`'+'` hamurinn getur verið gagnlegur þegar við viljum gera meira en einn hlut í einu. Segjum sem svo að við viljum opna skrá, prenta allar línurnar og síðan setja línu neðst:

```python
>>> with open('skra.txt','r+') as skra:
	      for i in skra:
		      print(i)
	      skra.write('\nþessi lína fer neðst\n')


Þetta er tilraun 1

Þetta er tilraun 2

Skrifaðu þetta neðst í skránnaÆ,æ, ég gleymdi að setja inn nýja línu

22
>>> with open('skra.txt','r') as skra:
	      for i in skra:
		      print(i)


Þetta er tilraun 1

Þetta er tilraun 2

Skrifaðu þetta neðst í skránnaÆ,æ, ég gleymdi að setja inn nýja línu



þessi lína fer neðst
```

Ef við viljum setja línu fremst í skránna þá getum við gert það svona :

```python
>>> with open('skra.txt','r+') as skra:
	      skra.write('þessi lína fer efst\n')


20
>>> with open('skra.txt','r') as skra:
	      for i in skra:
		      print(i)


þessi lína fer efst

Þetta er tilraun 2

Skrifaðu þetta neðst í skránnaÆ,æ, ég gleymdi að setja inn nýja línu



þessi lína fer neðst
```
Síðustu tvö dæmin sýna okkur að þegar við notum `'r+'` haminn þá opnast skráinn í fyrstu línu og allt sem skrifum inn í skránna fer fremst í skránna. Við getum síðan notað lykkju til þess að færa okkur neðst í skránna og síðan skrifa inn nýja línu þar.
