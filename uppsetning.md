# Að setja upp Python

Þegar hingað er komið skulum við ganga úr skugga um að við séum örugglega með Python á tölvunni okkar. Python fylgir alltaf með GNU/Linux stýrikerfum en aldrei með Windows eða macOS. Næstu köflum verður því skipt upp eftir stýrikerfi þar sem uppsetningarleiðbeiningarnar eru ekki eins né notum við sömu ritla.

### Windows
Þar sem að Python fylgir ekki með Windows þurfum við að byrja á að sækja uppsetningarskrá af vefsetri Python. Opnið vafra og sláið inn slóðina www.python.org/downloads og leitið að hnappi fyrir nýjustu útgáfuna. Þar ætti að standa `Download Python 3.x.x` þar sem x gefur til kynna útgáfunúmerið. Þegar þetta er skrifað þá er nýjasta útgáfan 3.7.0.

Smellið og hnappinn og halið niður viðkomandi skrá. Opnið möppuna þar sem skráin er vistuð (t.d. Downloads) og tvísmellið á skránna sem þið voruð að sækja.

Við það opnast uppsetningargluggi fyrir Python. Veljið `Install Now`. Þið gætuð þurft að staðfesta að það eigi að setja upp Python. Smellið á `Yes` og leyfið vélinni að vinna.

Þegar uppsetningu er lokið ætti að koma gluggi sem á stendur `Setup was successful`. Áður en þið lokið honum skulið þið smella á `Disable path length limit`. Það er gert svo að hægt sé að keyra Python skrár sem eru geymdar í hreiðruðum möppum.

Núna ætti að vera komið nýtt forrit á vélina sem heitir IDLE (Python 3.x 32-bit). IDLE virkar sem Python skel og við getum einnig notað forritið til þess að búa til python skrár (með endinguna .py) sem við notum til þess að halda utan um kóðana okkar.

Við þurfum ekki að nota IDLE til þess að keyra Python - við getum líka notað forrit sem heitir `Command Promt` sem fylgir með Windows. Smellið á Start hnappinn og leitið að `CMD`. Sláið inn `Python` í gluggann. Jafnvel þó að þetta forrit virki á mjög sambærilegan hátt og IDLE þá er það ekki mjög notendavænt og því skulum við halda okkur við að nota IDLE.

### macOS


### GNU/Linux
