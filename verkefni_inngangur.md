# Æfingaverkefni - lesa fyrst!

Í þessum hluta er að finna safn verkefna sem ætlað er að hjálpa til við að innbyrða og setja í samhengi þær upplýsingar sem fram koma í köflum bókarinnar.  
Markmiðið er að þannig verði ekki nauðsynlegt að setjast niður og lesa alla bókina til að geta byrjað á verkefnunum heldur lesum við okkur til um nauðsynlega þætti þegar við þurfum á því að halda. 
<br>
Hverju verkefni fylgir lýsing á virkni þess falls eða forrits sem við ætlum að skrifa. Það er jafn mikilvægt að átta sig á "vandamálinu" sem við ætlum að leysa og lausninni. Það getur reynst mjög gagnlegt að skrifa niður grófa áætlun eða drög að því hvernig verkefnið verði leyst og það þarf alls ekki að vera í kóða. Við breytum okkar eigin orðum í kóða síðar. Þau sem eru lengra komin geta jafnframt prófað að leysa verkefnið á eigin spýtur áður en þau skoða leiðbeiningarnar.
