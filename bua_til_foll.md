# Að búa til föll

Í þessum kafla ætlum við að kynnast föllum betur, hvað þau eru, hvernig við notum þau og auðvitað hvernig við getum búið til okkar eigin föll.

## Hvað er fall?

Til að átta sig á því hvað skilgreinir fall, þá er gagnlegt að velta fyrir sér tilgangi þeirra. Föll geta verið nýtt með mismunandi hætti eftir forritunarstíl hvers og eins.  
Sá eiginleiki sem við munum helst líta til er endurnýting kóða. Þegar við höfum skilgreint fall getum við kallað á það aftur og aftur, sama hversu langt og viðamikið fallið er þá þurfum við bara eina línu til að kalla á það.  
Til einföldunar fyrir byrjendur notum við hugtakið fall einnig sem einskonar regnhlífarhugtak og færum einnig undir það stefjur (e. procedures/subroutines) og aðferðir (e. methods). Munurinn á þessum þremur fyrirbærum er þess eðlis að hann skiptir ekki máli fyrir byrjendur. Samt sem áður getur verið gagnlegt að vita af þessum hugtökum, til dæmis ef leitað er í efni á veraldarvefnum þar sem frekar er kafað í smáatriði.  
Fólk sem aðhyllist stærðfræðilega nálgun við skilgreiningu hugtaksins fall gerir þá kröfu að föll taki alltaf inn eitthvað inntaksgildi og láti jafnframt frá sér skilagildi. Mæti „fallið“ ekki þessum kröfum sé um stefju að ræða.  
Hugtakið aðferð á rætur að rekja til hlutbundinnar forritunar (e. object-oriented programming) en í því samhengi má segja að aðferð sé fall (eða stefja) sem tilheyrir klasa (e. class).  
Einnig er vert að taka fram að í heiminum eru til fjölmargir tölvunarfræðingar og annað fólk sem komur að þróun forritunarmála. Það ætti því kannski ekki að koma á óvart að þetta fólk er ekki allt sammála um skilgreiningu þessara hugtaka og því getur verið áherslumunur eftir því hvers konar nálgun eða forritunarmál fólk aðhyllist. Það er einnig af þessum ástæðum sem einfaldara er fyrir byrjendur að kalla þetta allt *föll* til að byrja með.  

## Hvernig eru þau notuð?

Í Python-kóða má þekkja föll á svigunum sem fylgja nafni fallsins (dæmi: `print()`). Þegar við notum fall nefnist það *kall á fall* (e. function call). Sum föll taka inntaksgildi þegar kallað er á þau (jafnvel fleiri en eitt) á meðan önnur föll þurfa ekki inntaksgildi.  
Fallið `print()` er dæmi um fall sem hægt er að nota með eða án inntaksgildis. Ef við notum það án inntaksgildis skilar það auðri línu:
```python

>>> print()

```
Við getum einnig notað eitt inntaksgildi, bæði í formi gildis af einhverju tagi eða breytu. Gildið (eða breytan) er þá tilgreint innan sviga fallsins:

```python
>>> x = 'Mikilvæg skilaboð'
>>> print(x)
Mikilvæg skilaboð
>>> print('Önnur skilaboð')
Önnur skilaboð
```

Fallið `print()` getur tekið inn fleiri ein eitt inntaksgildi og þá aðgreinum við þau með kommu (`,`).

```python
>>> svar = 42
>>> print('Svar við öllu:', svar)
Svar við öllu: 42
```

## Hvernig búum við til föll?

Eins og fram kom í upphafi þessa kafla þá geta föll verið góð leið til að endurnýta kóða og viðhalda samfellu innan forritsins.  
Til að skilgreina fall notum við `def` setningu þar sem við tilgreinum nafn fallsins og mögulega stika sem fara á milli sviganna. Ef fallið notar ekki stika skiljum við svigana eftir tóma. Setningin endar svo á tvípunkti (`:`) og þar af leiðandi verður næsta lína hluti af bálki fallsins. Þessi uppsetning minnir eflaust einhverja [til dæmis] á *for* lykkjur og það er vegna þess að hér gilda sömu inndráttarreglur; í næstu línu á eftir tvípunkti skal inndráttur vera fjögur bil.  
Skoðum dæmi þar sem við skrifum fall sem prentar þrjá strengi:

### Mynd af fyrsta fallinu - ad_bua_til_fall_1.png

Prófum svo að kalla á fallið `hallo_heimur()`:

```python
>>> def hallo_heimur():
	print('*' * 20)
	print('Halló heimur!'.center(20))
	print('*' * 20)


>>> hallo_heimur()
********************
   Halló heimur!    
********************
```

Ef við viljum skilgreina fall sem notar stika (e. parameter) þurfum við einfaldlega að tilgreina það með því að setja stikann innan sviganna þegar við skilgreinum fallið. Það gerir okkur kleift að nota þennan stika í fallinu.

### Mynd af falli með stika - ad_bua_til_fall_2.png

Prófum næst að kalla á fallið `hallo_notandi()` og notum breytuna `nafn` sem inntaksgildi:

```python
>>> def hallo_notandi(nafn_notanda):
	print('Góðan daginn {}'.format(nafn_notanda))


>>> nafn = 'Blær'

>>> hallo_notandi(nafn)

Góðan daginn Blær
```

## Skilagildi

Við getum skilgreint föll þannig að þau skili af sér tilteknu gildi sem við köllum *skilagildi* (e. return value). Til að gera það notum við `return` setningu og við ákveðnar aðstæður gætum við viljað hafa margar slíkar í einu og sama fallinu, allar mismunandi eftir viðeigandi skilagildi. Þó er mikilvægt að hafa í huga að inni fallið `return` setningu lýkur keyrslu fallsins. Skilagildið getur verið af hvaða tagi sem er.  
Skoðum dæmi þar sem við útbúum fall (köllum það `reiknar_x_og_y`) sem tekur inn tvær tölur (`t1` og `t2`), skilgreinir breytuna `safn` sem er tómur listi og bætir útkomu nokkura reikniaðgerða á milli talnanna í listann. Að lokum skilar fallið listanum `safn`. Því má segja að *skilagildi* fallsins sé *listi*.  

```python
>>> def reiknar_x_og_y (t1, t2):
	safn = []
	safn.append(t1 + t2)
	safn.append(t1 - t2)
	safn.append(t1 * t2)
	safn.append(t1 / t2)
	return safn
```

Prófum því næst að skilgreina tvær breytur, `x` og `y`, til að nota sem inntaksgildi fallsins `reiknar_x_og_y`.

```python
>>> x = 4
>>> y = 7

>>> print(reiknar_x_og_y(x, y))

[11, -3, 28, 0.5714285714285714]
```

Eins og við sjáum, þá skilaði fallið okkur lista með fjórum stökum.  
Við getum einnig kallað á fallið með tveimur gildum án breyta.

```python
>>> print(reiknar_x_og_y(18, 9))

[27, 9, 162, 2.0]
```

## Gildissvið (e. scope)

Þegar við erum farin að skrifa okkar eigin föll þurfum við að hafa í huga fyrirbæri sem kallast gildissvið. Við getum hugsað okkur gildissvið sem ákveðnar samskiptareglur eða aðgangsstýringu innan forritsins.
Til að átta okkur betur á þessu skulum við taka sem dæmi fallið `reiknar_x_og_y` sem við bjuggum til í síðasta hluta. Í því falli er skilgreind ein breyta; listinn `safn`. Sjálfgefnar reglur um gildissvið í Python gera það að verkum að enginn annar hluti forritsins hefur aðgang að þessari breytu (`safn`).

### Tip
Reglur um gildissvið geta verið breytilega á milli forritunarmála.
