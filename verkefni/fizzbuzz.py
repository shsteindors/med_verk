# Verkefnið sem fall (Skilagildi = None)
def fizzbuzz(max_tala):
    for tala in range(max_tala+1):
        if tala % 3 == 0 and tala % 5 == 0:
            print('FizzBuzz')
        elif tala % 3 == 0:
            print('Fizz')
        elif tala % 5 == 0:
            print('Buzz')
        else:
            print(tala)


'''
Verkefnið þar sem hæsta- og lægsta tala koma inn
sem ílag (e. input) frá notanda
'''
h_tala = int(input('Sláðu inn hæstu töluna: '))
l_tala = int(input('Sláðu inn lægstu töluna: '))

for tala in range(l_tala, h_tala):
    if tala % 3 == 0 and tala % 5 == 0:
        print('FizzBuzz')
    elif tala % 3 == 0:
        print('Fizz')
    elif tala % 5 == 0:
        print('Buzz')
    else:
        print(tala)

