from random import randint

nafn_leikmanns = input('Hvað heitir þú? ')
hi = 100
lo = 1
leynitala = randint(lo, hi)
teljari = 0
leikur = True

while leikur == True:
    gisk = input('Giskaðu á tölu... ')  # Muna að breyta í heiltölu
    try:
        gisk = int(gisk)
        if gisk == leynitala:
            print('Vel gert {}, talan var einmitt {}!'.format(nafn_leikmanns,leynitala))
            print('{} þurfti {} tilraunir til að finna töluna.'.format(nafn_leikmanns, teljari))
            leikur = False
        elif gisk < leynitala:
            print('Talan er hærri!')
        elif gisk > leynitala:
            print('Talan er lægri!')
        teljari += 1
    except:
        print('Vinsamlegast sláðu inn HEILTÖLU á bilinu {} og {}!'.format(lo, hi))
