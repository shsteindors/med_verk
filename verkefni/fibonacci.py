def fibonacci_runa(tala):
    tala_1 = 0
    tala_2 = 1
    talnasafn = [tala_1, tala_2]
    for t in range(tala - 2):
        ny_tala = tala_1 + tala_2
        talnasafn.append(ny_tala)
        tala_1 = tala_2
        tala_2 = ny_tala
    return talnasafn

fjoldi = int(input('Fjöldi talna: '))

fib = fibonacci_runa(fjoldi)

for i in fib:
    print(i)
