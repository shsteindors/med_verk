"""
Hér er ein möguleg lausn fyrir palindrome verkefnið.
Lausn sem þessi (ekki nákvæmlega eins) myndi virka fyrir mörg forritunarmál.
"""
def palin_test(st_inn):
    lengd = len(st_inn)//2                         # þessi breyta mun gera range() hér að neðan læsilegra
                                               # við deildum með 2 því við þurfum bara að skoða helminginn (ATH //)
    for s in range(lengd):
        print(st_inn[s],'==', st_inn[-1 - s])  # þessi lína er óþörf en hjálpar okkur að sjá virknina
                                               # vísir '-1' er alltaf síðasta stakið (gott að muna)
        if not st_inn[s] == st_inn[-1 - s]:        # ef "spegluðu" stafirnir eru eins ...
            return False                           # ... þá höldum við áfram (for-lykkja keyrir aftur)                       # ... þá skilar fallið False (for-lykkja hættir)
    return True  
""" ef við komumst hingað hefur for-lykkjan náð að klára án
                     þess að fá False og þá skilar fallið True :) """ 

print(palin_test(input('Strengur: ')))


