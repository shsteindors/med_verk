* Geymslutög
  * listar
    * föll fyrir lista
      - index (ísl. vísir), append, remove/pop, sort
  * Túplar (fjölundir) "Línur" (e. tuples)
    - einkenni (vs. listar), hraði
  * Uppsláttarsöfn (e. dictionaries)
    - aðgreining frá listum
    - helstu föll

# Tög sem geyma önnur tög...

Flest forritunarmál bjóða upp leið til að halda utan um marga hluti í eins konar geymi (e. cointainer). Þessi geymir er af ákveðnu tagi (e. type) sem gerir okkur kleift að nálagst fleiri hluti sem hann heldur utan um. Það gerir hann með skipulögðum hætti og við getum nálgast "innihaldið" með því að gefa upp ákveðið heiltölu gildi, nokkurs konar sætisnúmer.
Til að átta sig örlítið betur á þessu má ímynda sér sjálfsala sem inniheldur til dæmis drykki eða nammi. Til að fá eitthvað úr sjálfsalanum sláum við inn töluna sem er fyrir framan þann hlut sem okkur langar í og sjálfsalinn skilar til okkar því sem við báðum um.

### MYND AF SJÁLFSALA

Í Python eru innbyggð tög sem hafa þessa eiginleika og í þessum hluta ætlum við að skoða lista (e. lists) og túpla (e. tuples). Við ætlum að reyna að svara eftirfarandi spurningum:

- Til hvers eru þessi tög?
- Hvað einkennir þau?
- Hvernig eru þau notuð?

# Listar (e. lists)

Byrjum á að skoða lista (e. list). Í stuttu máli má segja að listi sé breytanleg, röðuð runa af stökum. Hvað þýða þessi hugtök; *breytanleg*, *röðuð* og *runa*?  
Það að listi sé breytanlegur (e. mutable) þýðir að við getum breytt uppbyggingu (innihaldi) hans eftir að hann hefur verið búinn til. Við getum sem sagt bætt við, breytt og fjarlægt stök úr listanum. Þegar við segjum að listi sé röðuð runa mætti segja að það þýði að öll stök listans fá einskonar sætistölu og þessi röð helst þar til (og ef) við breytum henni sérstaklega. Einn af kostum *runu*-taga er að við getum ítrað (e. iterate) í gegnum þau til dæmis með for-lykkju.
Listar og strengir eiga það sameiginlegt að vera hvort tveggja runutök en eru þó ansi ólík tög. Strengir eru óbreytanlegir og halda einungis utan um stafi og tákn á meðan listar eru breytanlegir og geta innihaldið allskonar mismunandi tög, þar á meðal fleiri lista sem geta einnig innihaldið lista og svo framvegis.

Við vitum nú þegar að megin hlutverk lista er að halda utan um fleiri en einn hlut og að gera það með skipulögðum hætti. Þó er vert að nefna að listi getur verið tómur og við ákveðnar aðstæður gætum við kosið að búa til tóman lista. Við getum búið til tóman lista á tvo vegu; annars vegar með tómum hornklofum (`[]`) og hins vegar með fallinu `list()`:

```python
>>> tomur_listi = []
>>> tomur_listi
[]
>>> tomur_listi = list()
>>> tomur_listi
[]
```

Stökin sem listinn heldur utan um geta verið breytur eða einstaka gildi. Við getum einnig tilgreint stök listans þegar hann er skilgreindur (búinn til):

```python
>>> annar_listi = [123, 'strengur', 3.1415, True]
```

Til að nálgast stak úr lista skeitum við vísi (e. index) viðkomandi staks aftan við breytunafn listans. Prófum að prenta stak númer 1 úr síðasta dæmi:

```python
>>> print(annar_listi[1])
strengur
```

Eins og við sjáum, þá telja listar (alveg eins og strengir) frá núlli (fyrsti vísirinn er 0).  
Ef að breyta hefur heiltölugildi, þá getum við notað breytunafnið sem vísi:

```python
>>> einhver_tala = 1
>>> annar_listi[einhver_tala]
'strengur'
```

Við getum notað vísi listans til að breyta ákveðnu staki. Það gerum við með setningu þar sem gildinu er veitt á breytunafn listans ásamt vísi innan hornklofa:

```python
>>> annar_listi[0] = 321
>>> print(annar_listi)
[321, 'strengur', 3.1415, True]
```

### Sneiðing (e. slicing)

Við getum viljað vísa í mörg stök lista og notum til þess aðferð sem kallast sneiðing. Það gerum við, rétt eins og þegar við vísum í stakt stak, með því að tilgreina vísa innan hornklofa. Munurinn er sá að í stað þess að tilgreina einn vísi, þá tilgreinum við fyrsta vísinn sem við viljum fá ásamt þeim vísi sem við viljum fara að (næsta vísi á eftir þeim síðasta sem við viljum fá) og skiljum þá að með tvípunkti (`[x:y]`) innan hornklofanna. Þetta kann að virðast ruglingslegt í fyrstu en verður ljóst með því að skoða nokkur dæmi (og enn ljósara með því að prófa á eigin spýtur!).  

```python
>>> fleiri_tolur = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
>>> tolur_2til4 = fleiri_tolur[2:5]
>>> print(tolur_2til4)
[2, 3, 4]
```

Í þeim tilfellum þar sem þau stök sem við viljum liggja á öðrum hvorum enda, annað hvort frá upphafi og að ákveðnu staki eða frá ákveðnu staki og til enda, getum við sleppt því að tilgreina annann vísinn og Python les þá tvípunktinn þannig að spanið ná frá eða að endanum, eftir aðstæðum. Skoðum dæmi til að átta okkur betur á þessu:

```python
>>> fyrri_5 = fleiri_tolur[:5]
>>> print(fyrri_5)
[0, 1, 2, 3, 4]
>>> seinni_5 = fleiri_tolur[5:]
>>> print(seinni_5)
[5, 6, 7, 8, 9]
```

Annað sem vert er að nefna er vísirinn `-1` en hann táknar ávallt síðasta stak lista. Þannig er hægt að telja aftur á bak því `-2` er jafnframt næst-síðasta stakið og þannig koll af kolli.

```python
>>> print(fleiri_tolur[-1])
9
>>> print(fleiri_tolur[-3])
6
```

### Inniheldur listinn ákveðið stak?

Til að kanna hvort að listi innihaldi ákveðið stak getum við notað `in` virkjann. Ef við skrifum setningu með `in` virkjanum fáum við *Boole* skilagildi, annað hvort `True` eða `False`. Þannig erum við í raun að spyrja hvort að þetta stak finnist í þessum lista og svarið verður annað hvort *já* eða *nei*.

```
>>> x = ['a', 'b', 'c']
>>> print('a' in x)
True
>>> print('f' in x)
False
```

### Samskeyting og margföldun listar

Við getum skeytt saman tveimur eða fleirum listum með því að setja plúsmerki á milli þeirra:  

```python
>>> a = [1,2,3]
>>> b = [4,5,6]
>>> c = [7,8,9]
>>> saman = a + b + c
>>> print(saman)
[1, 2, 3, 4, 5, 6, 7, 8, 9]
```

Við getum einnig búið til lista sem endurtekin stök annars lista:

```Python
>>> margfoldun = annar_listi * 2
>>> print(margfoldun)
[123, 'strengur', 3.1415, True, 123, 'strengur', 3.1415, True]
```

Við getum fjarlægt stök úr lista og höfum til þess nokkrar aðferðir.  
Við ætlum að skoða þrjár leiðir:  

* `pop()`
* `del`
* `remove()`

Í tilfellum þar sem við viljum nota vísi staks til að eyða því getum við við notað innbyggt fall fyrir lista sem heitir `pop()`. Fallinu er skeytt aftan við breytunafn listans og tekur inntaksgildi, heiltölu, sem er vísir þess staks sem skal fjarlægt.

```python
>>> slettar_tolur = [2, 14, 68, 111, 248]
>>> slettar_tolur.pop(3)
111
>>> print(slettar_tolur)
[2, 14, 68, 248]
```

Takið eftir því að þegar við notum `pop()`, þá prentast sjálfkrafa stakið sem við fjarlægðum.  

Önnur aðferð til að fjarlægja stök með því nota vísa þeirra er að nota `del` setningu.  
Þegar við notum `del` eru stökin ekki prentuð eins og með `pop()` en það er vegna þess að `del` getur fjarlægt mörg stök í einni setningu. Til að fjarlægja mörg stök í einu notum við sneiðingu. Skoðum nokkur dæmi:

```python
>>> del fleiri_tolur[7]  # Eyðum staki með vísi 7
>>> print(fleiri_tolur)
[0, 1, 2, 3, 4, 5, 6, 8, 9]
>>> del fleiri_tolur[7:]  # Eyðum öllu frá vísi 7
>>> print(fleiri_tolur)
[0, 1, 2, 3, 4, 5, 6]
```

Í tilfellum þar sem vísir þess staks sem við viljum fjarlægja er óþekktur getum við notað fallið `remove()`. Þessu falli er skeytt aftan við breytunafn lista (`listi.remove()`) en í stað þess að taka vísi staks sem inntaksgildi athugar fallið hvort listinn innihaldi stak sem er eins og inntaksgildið og fjarlægir það (ef það finnst).

```python
>>> allskonar_hlutir = [1337, 'kettlingur', 3.14, 'hamstur']
>>> allskonar_hlutir.remove(3.14)
>>> print(allskonar_hlutir)
[1337, 'kettlingur', 'hamstur']
>>> allskonar_hlutir.remove('hamstur')
>>> print(allskonar_hlutir)
[1337, 'kettlingur']
```

## Fleiri algeng föll fyrir lista

Í þessum hluta höfum við kynnst ýmsum föllum í tengslum við ákveðnar aðgerðir. Hér á eftir komum við inn á nokkur föll til viðbótar, bæði föll sem eru sérstaklega fyrir lista en einnig föll sem notuð eru með fleiri gagnatögum en gagnlegt er að vita hvernig virka með listum.

### Bætum við staki með append()

Fallið `append()` bætir staki aftast í lista.  

```python
>>> kattategundir = ['Persian', 'Maine Coon', 'Bengal']
>>> kattategundir.append('Sphynx')
>>> print(kattategundir)
['Persian', 'Maine Coon', 'Bengal', 'Sphynx']
```

### Teljum fjölda staka með count()

Til að telja hversu oft eitthvað tiltekið stak kemur fyrir í lista getum við notað fallið `count()`. Fallinu er skeytt aftan við lista og þegar kallað er á fallið þarf að tilgreina það stak sem skal talið (`listi.count(<stak>)`). Strengurinn sem fallið tekur inn (leitarstrengurinn) getur verið staflesgildi að lengd eða lengri. Mikilvægt er að hafa í huga að fallið er hástafanæmt (e. case sensitive). Skilagildi fallsins er heiltala og ef ekkert tilvik finnst, þá verður skilagildið núll (`0`).

```python
>>> einhver_listi = [1,4,5,1,3,3,5,6,2,9,3,7,3]
>>> einhver_listi.count(1)
2
>>> einhver_listi.count(3)
4
>>> einhver_listi.count(8)
0
```

### Bætum við á ákveðinn vísi með insert()

Fallið `insert()` færir stak inn í lista á ákveðinn vísi. Það þýðir að í stað þess að bæta stakinu aftast í listann (eins og `append() gerir`) þá veljum við staðsetningu staksins innan listans.  
Fallið tekur tvö inntaksgildi, annars vegar vísinn (sætistöluna) og hins vegar það gildi sem á að bæta inn í listann (`listi.insert(<vísir>, <gildi>)`). Stök með vísi hærri en nýja stakið færast einu sæti aftar (*vísir + 1*). Engu staki er eytt úr listanum.

```python
>>> a = [1,2,3,4]
>>> a.insert(2, 'kisa')
>>> print(a)
[1, 2, 'kisa', 3, 4]
```

### Röðum stökum með sort()

Fallið `sort()` raðar stökum lista eftir stærð eða í stafrófsröð. Fallið getur ekki raðað innan lista sem innihalda bæði tölur og runur (t.d. strengi og heiltölur). Ef listinn inniheldur tölur, þá raðast þær eftir stærð og ef hann inniheldur strengi, þá raðast þeir í stafrófsröð.  

Dæmi þar sem stökin eru heiltölur:  

```python
>>> b = [2, 3, 1, 6, 34, 16]
>>> print(b)
[2, 3, 1, 6, 34, 16]
>>> b.sort()
>>> print(b)
[1, 2, 3, 6, 16, 34]
```

Dæmi þar sem stök eru strengir:

```python
>>> kattategundir = ['Persian', 'Maine Coon', 'Bengal', 'Sphynx']
>>> kattategundir.sort()
>>> kattategundir
['Bengal', 'Maine Coon', 'Persian', 'Sphynx']
>>> print(kattategundir)
['Bengal', 'Maine Coon', 'Persian', 'Sphynx']
```

Fallið er hægt að nota án inntaksgilda og þá virkar það eins og því var lýst hér á undan. Fallið getur aftur á móti tekið inn eitt eða tvö inntaksgildi; `key` og `reverse`. Gildið `reverse` er Boole gildi og við getum skilgreint það sem `True` og þá raðast stökin í öfugri röð. Gildið `key` býður upp á að raða einnig eftir ákveðnum lykilþáttum. Í dæminu hér á eftir ætlum við að skilgreina leng (`len`) sem lykilatriði og þá raðast strengir ekki lengur í stafrófsröð (sem er sjálfgefið) heldur eftir lengd.  

```python
>>> kattategundir.sort(reverse=True)
>>> print(kattategundir)
['Sphynx', 'Persian', 'Maine Coon', 'Bengal']
```

```python
>>> kattategundir.sort(key=len)
>>> print(kattategundir)
['Bengal', 'Sphynx', 'Persian', 'Maine Coon']
```

### Öfug röð með reverse()

Fallið `reverse()` endurraðar stökum innan lista og speglar þá röð sem stökin voru í fyrir. Öfugt við `sort()`, þá raðar `reverse()` stökum ekki eftir neinni reglu eins og stærðarröð eða stafrófsröð, bara öfuga röð frá þeirri sem fyrir var.

```python
>>> avextir = ['epli', 'ananas', 'banani', 'vatnsmelóna']
>>> avextir.reverse()
>>> print(avextir)
['vatnsmelóna', 'banani', 'ananas', 'epli']
```

## Föll sem eru ekki bara fyrir lista

Þessi föll eru ekki sérstaklega fyrir lista en geta engu síður reynst nytsamlega fyrir lista.

### len()

Fallið `len()` skilar heiltölugildi sem segir til um fjölda staka innan viðkomandi lista.

```Python
>>> einhver_listi = [1,4,5,1,3,3,5,6,2,9,3,7,3]
>>> len(einhver_listi)
13
```

### max()

Fallið `max()` skilar hæsta *tölugildi* lista og er einungis fyrir tölur (heil- eða kommutölur).

```Python
>>> einhver_listi = [1,4,5,1,3,3,5,6,2,9,3,7,3]
>>> max(einhver_listi)
9
```

### min()

Fallið `min()` skilar lægsta *tölugildi* lista og er, rétt eins og `max()`, einungis fyrir tölur (heil- eða kommutölur).

```Python
>>> einhver_listi = [1,4,5,1,3,3,5,6,2,9,3,7,3]
>>> min(einhver_listi)
1
```

# Túplar (e. tuples)

Túpull er runutag rétt eins og listi. Gildi sem geymd eru í túpli geta verið af hvaða tagi sem er og hafa sætistölu; vísi. Einhver kann að hugsa með sér að túpull sé þá bara alveg eins og listi? Túpull og listi eiga margt sameiginlegt en á þeim er þó einn mjög mikilvægur grundvallar munur; túpull er óbreytanlegur.  
Hvað þýðir það að túpull sé óbreytanlegur?  
Það þýðir að eftir að hann hefur verið skilgreindur, þá er ekki hægt að breyta honum.  
Þá er spurningin hvers vegna við myndum vilja nota túpul þegar við getum notað lista? Listar eru jú fjölhæfari. Meginástæðurnar eru tvær; annars vegar að túplar eru mun hraðari í meðhöndlun en listar og í tilfellum þar sem unnið er með stór gagnasöfn getur munað miklu á vinnslutíma. Hins vegar geta falist kostir í því að gögn geti ekki breyst og það hefur í för með sér ákveðið öryggi.  

Hvað málskipan varðar þá eru túplar skilgreindir með svigum en ekki hornklofum eins og listar. Stök innan túpla eru aðskilin með kommum rétt eins og innan lista.

```python
>>> a = (1, 2, 3, 4)
>>> type(a)
<class 'tuple'>
```

### Breytum lista í túpul og öfugt

Við getum breytt lista í túpul með fallinu `tuple()` og notum viðkomandi lista sem inntaksgildi.

```python
>>> x = [1, 2, 'a', 'b']
>>> type(x)
<class 'list'>
>>> x = tuple(x)
>>> type(x)
<class 'tuple'>
```

Á sama máta getum við einnig breytt túplum í lista með `list()` fallinu.

```python
>>> type(x)
<class 'tuple'>
>>> x = list(x)
>>> type(x)
<class 'list'>
```

Með túplum getum notað flest föll sem eru fyrir lista svo lengi sem þau föll eru ekki til að breyta listanum. Við getum til dæmis notað föll eins og `count()` til að telja stök innan túpulsins en við getum ekki notað fall eins og `append()` sem bætir staki við lista. Ef við reynum það fáum við villuboð sem útskýra það fyrir okkur:

```python
>>> t = (1, 2, 3, 4)
>>> t.append(5)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'tuple' object has no attribute 'append'
```

# Tætiföflur (e. hash-tables / Python Dictionaries)

Tætitafla (e. hash table) er gagnatag sem byggir á pörun á milli lykils (e. key) og gildis (e. value). I Python eru tætitafla kölluð *dictionary* (ísl. *orðabók* eða *uppflettirit*) frekar en *hash table* þó svo að hugtökin beri sömu merkingu. Við munum hins vegar sjá að þetta gagnatag á ýmislegt sameiginlegt með orðabókum og því er kannski skiljanlegt að Python fólkið hafi frekar viljað nota það hugtak.

Tætitafla er breytanlegt og óraðað tag. Við segjum að hún sé breytanleg vegna þess að við getum breytt gildum hennar, bætt við og fjarlægt eftir að hún hefur verið skilgreind. Við segjum að tætitaflan sé óröðuð vegna þess að stök hennar hafa ekki vísi sem er heiltölugildi eins og til dæmis listar. Þess í stað má segja að að lyklar tætitöflunnar séu vísar en þeir, ásamt gildum þeirra, halda ekki endilega alltaf sömu röð. Hvert stak tætitöflunnar er par lykils og gildis.

Tætitöflur eru skilgreindar með slaufusvigum (`{}`), lykill og gildi eru aðskilin með tvípunkti og pör með kommu.

```python
>>> nemandi = {'nafn':'Blær', 'aldur':15, 'greinar':['stærðfræði', 'íslenska']}
```

Til að búa til tóma tætitöflu getum annaðhvort skilgreint breytu með tómum slaufusvigum eða með fallinu `dict()`:

```python
>>> x = {}
>>> type(x)
<class 'dict'>
>>> y = dict()
>>> type(y)
<class 'dict'>
```

### Hvernig nálgumst við gögn úr tætitöflu?

Eins og fram hefur komið, þá hafa stök tætitöflunnar engin föst sætisnúmer eins og stök lista. Þess í stað höfum við lykla og getum notað þá eins og vísa.  
Lyklar tætitöflunnar geta verið af ýmsu tagi; heil- og kommutölur, strengir eða túplar. Gildi lyklanna geta hins vegar verið af hvaða tagi sem er til dæmis listar eða tætitöflur.  
Skoðum dæmi þar sem við viljum prenta gildi lykilsins `'aldur'` úr tætitöflunni `nemandi` hér á undan:

```python
>>> print(nemandi['aldur'])
15
```

En hvað gerist ef við vísum á lykil sem ekki er í tætitöflunni?

```python
>>> print(nemandi['sími'])
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
KeyError: 'sími'
```

Eins og við sjáum, þá fáum við villuboð ef við vísum á lykil sem ekki er til staðar. Stundum viljum við ekki að forrit stöðvist með villuboðum og þá þurfum við að gera ákveðnar ráðstafanir og við köllum það villumeðhöndlun (e. error management).  
Skoðum næst aðra leið þar sem við notum fall fyrir tætitöflur sem heitir `get()`. Þetta fall athugar hvort að sá lykill sem við erum að leita að sé í tætitöflunni. Sé viðkomandi lykill í tætitöflunni, þá skilar fallið gildi lykilsins en annars skilar það tómagildinu `None`.

```python
>>> print(nemandi.get('sími'))
None
```

Einnig býður `get()` upp á valkvæmt auka inntaksgildi sem er strengur. Þessi strengur verður að skilagildi fallsins í stað `None` ef lykillinn finnst ekki.  

```python
>>> print(nemandi.get('sími', 'Lykill finnst ekki :('))
Lykill finnst ekki :(
```

### Hvernig bætum við stökum inn í tætitöflu?

Til að bæta nýju staki í tætitöflu skeytum við hornklofum aftan við breytunafn viðkomandi tætitöflu, innan hornklofanna setjum við lykilinn, eftir hægri hornklofann kemur jafnaðarmerki (`=`) og að lokum gildið.  

Skoðum dæmi þar sem við bætum lykli sem er strengurinn `sími` í tætitöfluna `nemandi` ásamt gildi sem er strengurinn `999-9999`:

```python
>>> nemandi['sími'] = '999-9999'
>>> print(nemandi)
{'nafn': 'Blær', 'aldur': 15, 'greinar': ['stærðfræði', 'íslenska'], 'sími': '999-9999'}
```



### Hvernig uppfærum við gildi?

Til að uppfæra gildi getum við notað sömu aðferð og við beittum í síðasta dæmi. Ímyndum okkur að nemandinn hafi fengið nýtt símanúmer og við viljum uppfæra það. Nýja símanúmerið er `999-9990`:

```python
>>> nemandi['sími'] = '999-9990'
>>> print(nemandi)
{'nafn': 'Blær', 'aldur': 15, 'greinar': ['stærðfræði', 'íslenska'], 'sími': '999-9990'}
```

### Hvernig athugum við hvort ákveðinn *lykill* sé í tætitöflu?

Til að athuga hvort að ákveðinn lykill finnist í tætitöflu getum við notað `in` virkjann.  Ef við skrifum setningu með `in` virkjanum fáum við *Boole* skilagildi, annað hvort `True` eða `False`.

```python
>>> 'sími' in nemandi
False
>>> 'aldur' in nemandi
True
```

### Hvernig athugum við hvort tiltekið *gildi* finnist í tætitöflu?

Til að athuga hvort að tiltekið gildi sé að finna í tætitöflu getum við notað `in` virkjann alveg eins og ef við værum að gá að lykli. Í þessu tilfelli þurfum við þó að fara aðeins öðruvísi að heldur ef við værum að athuga með lykil. Skoðum dæmi þar sem við ætlum að kanna hvort að nafnið (eða strengurinn) `Blær` komi fyrir. Prófum að beita sömu aðferð og ef um lykil væri að ræða:  

```python
>>> print(nemandi)
{'nafn': 'Blær', 'aldur': 15, 'greinar': ['stærðfræði', 'íslenska']}
>>> 'Blær' in nemandi
False
```

Eins og við sjáum, þá fáum við `False` jafnvel þó við vitum að gildið `Blær` sé til staðar í tætiföflunni `nemandi`. Ástæðan er sú að þegar við notum `in` virkja með tætitöflu, þá á það alltaf við um lykla nema að við tökum annað sérstaklega fram. Það gerum við með því að nota fall fyrir tætitöflur sem heitir `values()`. Ef við skeytum því aftan við breytunafn tætitöflunnar í `in` setningunni þá getum við leitað í gildum frekar en lyklum. 

```python
>>> 'Blær' in nemandi.values()
True
```

### Hvernig eyðum við staki úr tætitöflu?

Til að eyða staki getum við notað `del` setningu. Segjum sem svo að við viljum ekki lengur halda utan um símanúmer nemenda og viljum því fjarlægja lykilinn `sími` og gildið sem honum fylgir. 

```python
>>> del nemandi['sími']
>>> print(nemandi)
{'nafn': 'Blær', 'aldur': 15, 'greinar': ['stærðfræði', 'íslenska']}
```

Einnig gætum við notað fallið `pop()` til að fjarlægja stak úr tætiföflu. Þetta sama fall er einnig í boði fyrir lista og hefur sömu virkni fyrir tætitöflur; það tekur lykil sem inntaksgildi og það prentar síðan gildi þess staks sem við fjarlægjum.  

Skoðum dæmi þar sem við endurtökum síðasta dæmi nema með `pop()` fallinu:

```python
>>> nemandi.pop('sími')
'999-9990'
>>> print(nemandi)
{'nafn': 'Blær', 'aldur': 15, 'greinar': ['stærðfræði', 'íslenska']}
```

### Fjöldi staka með fallinu *len()*

Fallið `len()` skilar heiltölugildi sem segir til um fjölda staka innan viðkomandi tætitöflu þar sem hvert stak er par lykils og gildis (`lykill : gildi`).

```python
>>> print(nemandi)
{'nafn': 'Blær', 'aldur': 15, 'greinar': ['stærðfræði', 'íslenska']}
>>> print(len(nemandi))
3
```

### Um *for* lykkjur og tætitöflur

Við getum ítrað (e. iterate) í gegn um tætitöflu alveg eins og um lista væri að ræða og þá fer lykkjan á milli lykla tætitöflunnar.  

```python
>>> for x in nemandi:
...     print(x)
... 
nafn
aldur
greinar
```

Ef við viljum prenta gildi tætitöflunnar með *for* lykkju, þá skulum við rifja upp hvernig við nálgumst gildi staks. Það gerum við með því að setja lykilinn innan hornklofa aftan við breytunafn tætitöflunnar `<tætitafla>[<lykill>]`.  

Ef við horfum á síðasta kóðadæmi þar sem við notuðum *for* lykkjuna, þá sjáum við að breytan `x` er lykill tætitöflunnar hverju sinni. Það þýðir að við getum sett innan hornklofa aftan við `nemandi`:

```python
>>> for x in nemandi:
...     print(nemandi[x])
... 
Blær
15
['stærðfræði', 'íslenska']
```

Við gætum einnig viljað prenta saman lykil og gildi:

```python
>>> for x in nemandi:
...     print(x, ':', nemandi[x])
... 
nafn : Blær
aldur : 15
greinar : ['stærðfræði', 'íslenska']
```

### Lykill og gildi saman með *items()*

Önnur leið til að prenta lykil og gildi saman væri að nota `items()` fallið. Munurinn er sá að `items()` skilar tveimur gildum og það þýðir að við getum notað tvær breytur í *for* lykkjunni; eina fyrir lykilinn og eina fyrir gildið.

```python
>>> for x, y in nemandi.items():
...     print(x, ':', y)
... 
nafn : Blær
aldur : 15
greinar : ['stærðfræði', 'íslenska']
```


