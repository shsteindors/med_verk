# Efnisþættir/-yfirlit

 * Fyrsta forritið
 * Gagnatög
    * Fyrstu gagnatögin:
        * heiltölur (e. integers)
        * kommutölur (e. floats)
            * reikniaðgerðir
        * strengir
            * föll fyrir strengi
	            - index
            * "sneiðing" (e. slicing)
            * samskeyting/sameining (+)
        * bool(ean)
    * Geymslutög
        * listar
            * föll fyrir lista
	            - index (ísl. vísir), append, remove/pop, sort
        * Túplar (fjölundir) "Línur" (e. tuples)
	        - einkenni (vs. listar), hraði
        * Uppsláttarsöfn (e. dictionaries)
	        - aðgreining frá listum
	        - helstu föll

 * Lykkjur og skilyrðingar:
    * for
	    - fallið - `range()`
    * if
    * while
 * Flytja inn gögn
 * Flytja út gögn
 * Myndir
    * turtle (svipað og Logo)
    * Notendaviðmót (Tk / ~~Qt~~)
	    - Mögulega skoða *Gooey* og *Wooey*
    * Myndræn framsetning gagna (matplotlib)

## Fyrsta forritið

Það er hefð fyrir því að fyrsta forritið sem fólk skrifar sé einfalt forrit sem prentar `Hello World` á skjáinn. Það getur verið áhugavert að skoða muninn á þessu einfalda forriti á milli ólíkra forritunarmála þar sem forritin geta verið misjafnlega einföld. Við skulum viðhalda þessari hefð og skrifa eitt slíkt forrit fyrir Python:

```python
print("Halló heimur")
```
Ef við skoðum hvernig kóðinn fyrir forritið liti út í Java, öðru vinsælu forritunarmáli, þá gæti það forrit litið einhvern veginn svona út:

```java
public class halloHeimur
{
	public static void main(String[] args)
	{
		System.out.println("Halló heimur");
	}
}
```
Eins og við sjáum er Python kóðinn talsvert styttri og einfaldari. Það þýðir auðvitað ekki að Java sé verra forritunarmál en Python, heldur getur það reynst heppilegra við ákveðnar aðstæður. Mismunandi verkfæri henta mismunandi verkefnum. Þess vegna læra sumir fleiri en eitt forritunarmál og safna þannig fleiri verkfærum í verkfærakistuna sína. Við myndum sennilega ekki vilja ráða smið sem neitar að nota önnur verkfæri en hamar. Það er hægt að gera margt með hamri en ekki allt!

## Þetta eru allt hlutir...

**Nefna breytur og hluti, án þess þó að fara beint inn í hlutbundna forritun.**

## Fyrstu gagnatögin

Í þessum hluta munum við byrja á að skoða einföldustu gagnatögin í Python.

* Tölustafir
	* Heiltölur
	* Hlaupakommutölur
* Strengir
* Boole


### Tölustafir

Í mörgum forritunarmálum er ekki nóg að henda einhverri tölu inn í kóðann og ætlast til að allt virki. Við slíkar aðstæður þurfum við einnig að tilgreina hvers konar tölur við erum með. Það geta verið heiltölur (e. integers) eða hlaupakommutölur (e. floating-point numbers) og þessar tegundir skiptast jafnvel í nokkrar gerðir taga (e. types). Dæmið hér á eftir er úr Java.

```java
int tala1 = 1337;
double tala2 = 3.1415926535;
```

Í Python þurfum við aftur á móti ekki að tilgreina sérstaklega tag talna þegar við skilgreinum („búum til“) breytur.

```python
tala1 = 1337
tala2 = 3.1415926535
```

Í síðasta dæminu verður breytan `tala1` heiltala (int) og þar sem seinni talan innheldur kommu, þá verður `tala2` skilgreind sem kommutala (float). Við getum þurft að tilgreina tag ef við viljum breyta því síðar (eftir að breytan hefur verið skilgreind).

#### Heiltölur (e. integers)

Heiltölur eru allar tölur sem hefur ekki kommu (heil tala) en þær geta líka verið neikvæðar. Það gæti þvi verið 5, 1337, -99 eða bara 0.  
Eitt af því sem Python 3 hefur fram yfir Python 2 er hversu stórar heiltölur geta orðið. Í Python 2 er venjuleg heiltala (int) 32-bita en það þýðir að hún getur verið á bilinu -2,147,483,648 til 2,147,483,647. Af hverju er neikvæða talan stærri? Vegna þess að núll (0) telst jákvæð. Python 2 hefur einnig heiltölu gagnatag sem heitir *long* sem býður upp á 64-bita tölur (2<sup>63</sup>). Þetta er svipað og í Java forritunarmálinu þar sem heiltölur (int) eru sjálfgefið 32-bit en svo eru önnur tög eins og *long* (64-bita), *short* (16-bita) og *byte* (8-bita).  
Í Python 3 er þetta talsvert einfaldara og þar er heiltalan bara eitt tag (int) og hún getur verið meira 64 bita. Tökum sem dæmi töluna Googol  sem er 10<sup>100</sup> (tíu í veldinu hundrað!).

```python
>>> googol = 10**100
>>> googol
10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
>>> googol * googol
100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
>>>
```

Við munum hins vera ekki þurfa á svo stórum tölum að halda í bráð.


#### Hlaupakommutölur (e. floating-point numbers / "floats")

Hlaupakommutölur hafa, öfugt við heiltölur, **kommu** og minnst einn aukastaf. Fyrir kommutölu (float) er fimm ekki bara fimm, heldur fimm-komma-núll (5,0).



#### Reikniaðgerðir

Við getum notað Python til að framkvæma alla útreikninga. Við þurfum bara að vita hvernig við útskýrum dæmið svo Python skilji það. Hér að neðan er tafla með einföldustu virkjunum en við lærum meira um útreikninga síðar.  

| Virki | Lýsing - virkni        | Dæmi   | Útkoma |
|-------|------------------------|--------|--------|
| +     | Samlagning             | 9 + 5  | 14     |
| -     | Frádráttur             | 9 - 5  | 4      |
| *     | Margföldun             | 9 * 5  | 45     |
| /     | Deiling                | 9 / 5  | 1.8    |
| //    | Heiltöludeiling        | 9 // 5 | 1      |
| %     | Stofnleif (e. modulus) | 9 % 5  | 4      |
| **    | Veldisreikningur       | 9 ** 5 | 59049  |

Líklega kannast flestir við fyrstu fjóra virkjana í töflunni en við skulum fara aðeins yfir hina þrjá. *Heiltöludeiling* virkar þannig að við deilingu verða engir aukastafir í útkomunni (við munum að heiltölur vilja engar kommur).  
Stofnleif virkar þannig að útkoman verður afgangurinn eftir deilingu. Ef við hugsum um dæmið í töflunni (9 % 5), þá virkar það eins og heiltöludeiling en í stað þess að fá að vita hversu oft fimm gengur upp í níu, þá fáum við að vita hversu mikið verður eftir.  
Veldisreikningur er líklega mörgum kunnugur en táknin (\*\*) eru kannski ekki jafn kunnugleg. Algengt er að níu í fimmta veldi sé skrifað 9<sup>5</sup> og á vasareiknum er táknið oft uppfleygur `9^5`. Reikniaðgerðin níu í fimmta veldi er í raun `9 * 9 * 9 * 9 * 9 = 59049`.  

#### Röð aðgerða

Þegar við framkvæmum útreikninga þar sem notast er við fleiri en einn virkja, til dæmis þar sem bæði er samlagning og margföldun, þá eru ákveðnar aðgerðir framkvæmdar á undan öðrum. Til dæmis er margföldun framkvæmd á undan samlagningu.  
<br>
Skoðum einfalt dæmi:
```python
	>>> 1 + 2 * 3
	7
```
Af hverju er svarið sjö? Það er vegna þess að margföldunin er framkvæmd á undan samlagningunni og því er dæmið í raun `1 + 6`. Svarið hefur líklega ekki komið mörgum á óvart því þetta er algjörlega samkvæmt því sem við höfum lært í stærðfræði. Ef við viljum framkvæma samlagninguna fyrst, þá getum við notað sviga:  
```python
	>>> (1 + 2) * 3
	9
```
<br>
Við getum alltaf notað sviga ef við viljum vera alveg viss um að röð aðgerða sé eins og við ætluðum. Það getur verið gagnlegt þegar útreikningar eru orðnir flóknari en dæmin hér á undan. Auka svigar skaða ekki og við getum prófað fyrra dæmið aftur með óþarfa svigum:
```python
	>>> 1 + (2 * 3)
	7
```

#### Tagskipti (e. type conversion)

Ef við notum heiltölur og kommutölur saman í reikniaðgerð verður útkoman alltaf kommutala. Einnig fáum við alltaf kommutölu við deilingu, jafnvel þó svo að báðar tölurnar séu heiltölur. Ef við viljum ekki fá kommutölu þurfum við að nota heiltöludeilingu.

Við getum breytt heiltölu í kommutölu ef þörf er á. Það sem gerist, fyrir utan að gagnatagið breytist, er að talan fær kommu.  

Við getum líka breytt kommutölu í heiltölu. Þá þurfum við hins vegar að hafa í huga að heiltölur eru ekki eins nákvæmar og kommutölur og við tagskiptin gilda ekki sömu reglur um námundun og við lærðum í stærðfræði. Til að breyta í heiltölu notum við fallið `int()` og til að breyta í kommutölu notum við fallið `float()`. Við getum bæði notað þessi föll á breytur og einstaka tölur. Skoðum dæmi:
```python
	>>> int(0.9999999999999999)
	0
	>>> int(0.99999999999999999)
	1
	>>> x = 1337
	>>> float(x)
	1337.0
	>>>
```


### Boole-tag (e. boolean)

Boole-tög eru einföld fyrirbæri að því leyti að þau geta bara haft tvö möguleg gildi, satt eða ósatt. Í Python er það `True` eða `False`.
Þau eru kennd við stærðfræðinginn [George Boole](https://en.wikipedia.org/wiki/George_Boole) og kemur kemur nafnið þaðan.  
*Boole-segðir* leiða af sér gildin `True` eða `False`. Skoðum dæmi (tvær segðir) þar sem við notum virkjann `==` til að bera saman tvö gildi og niðurstaðan verður annað hvort `True` eða `False`:  

```python
>>> 1 == 1
True
>>> 1 == 2
False
>>>
```

Í dæminu hérna áðan notuðum við virkjann `==` en hann er einn af nokkrum samanburðarvirkjum (e. comparison operators).  
Hér sjáum við hina:

|                   |                           |
|-------------------|---------------------------|
|```x != y  ```     | x er ekki jafnt og y      |
|```x > y  ```      | x er meira en y           |
|```x < y  ```      | x er minna en y           |
|```x >= y  ```     | x er meira eða jafnt og y |
|```x <= y  ```     | x er minna eða jafnt og y |
|```x is y  ```     | x er *það sama* og y      |
|```x is not y  ``` | x er *ekki það sama* og y |

`True` og `False` eru sérstök gildi sem tilheyra taginu `bool` og þau eru **ekki** strengir.

```python
>>> type(True)
<class 'bool'>
>>> type(False)
<class 'bool'>
>>>
```


### Strengir (e. strings)

Strengir eru fyrsta dæmið um runur í Python (e. squence) sem við skoðum.  Síðar skoðum við önnur tög sem geta innihaldið runur af hverju sem er en <u>strengir eru runur prenttákna</u> (bókstafa, tölustafa eða annarra tákna).  
Í Python eru strengir óbreytanlegir (e. immutable) sem þýðir að þegar við höfum búið til streng, þá getum við ekki breytt honum. Við getum hins vegar afritað hann með breytingum en við skoðum það einmitt í þessum hluta. Það er misjafnt á milli ólíkra forritunarmála hvort strengir, eða önnur gagnatög, séu óbreytanleg.  

#### Hvernig búum við til strengi?

Við búum til strengi í Python með því að setja tilvitnunarmerki (gæsalappir) utan um textann eða táknin sem eiga að verða strengur.  
Í Python þurfum við ekki að tilgreina sérstaklega að breyta eigi að vera  strengur. Ef við setjum tilvitnunarmerki utan um gildi breytunnar, þá ákveður Python að þar sé strengur á ferðinni:

```python
>>> strengur = "Ég er strengur :)"
```

Strengir geta líka verið tómir (e. empty string).

```python
>>> tomur_strengur = ""
```

Við getum bæði notað högg (') eða tvíhögg (") en megum þó ekki blanda þeim saman. Python notar alltaf högg (einföld, ekki tvíhögg). Við skulum skoða nokkur dæmi til að átta okkur betur á þessu:  

```python
	>>> 'Kátir'
	'Kátir'
	>>> "krakkar"
	'krakkar'
```

Hérna prófum við að blanda þeim saman:

```python
>>> " Ha? Má þetta ekki? '
  File "<stdin>", line 1
    " Ha? Má þetta ekki? '
                          ^
SyntaxError: EOL while scanning string literal
```

Python túlkurinn lætur okkur vita að þetta gangi ekki upp.  
Við fáum einnig athugasemd ef við notum sömu tilvitnunarmerkin utan um strenginn og inni í strengnum.  

```python
>>> "Jújú, þetta var bara "fínt""
  File "<stdin>", line 1
    "Jújú, þetta var bara "fínt""
                              ^
SyntaxError: invalid syntax
```
Til að laga þetta höfum við tvo möguleika. Við gætum skipt ytri tvíhöggunum út fyrir stök högg eða notað tvíhögg utan um strenginn og þá einföld inni í honum (sjá dæmið hér að neðan). Þetta er ástæða þess að Python styður bæði táknin fyrir strengi:

```python
>>> 'Jújú, þetta var bara "fínt"'
'Jújú, þetta var bara "fínt"'
```

Hins vegar getum við notað svokallaðan lausnarstaf (e. escape character) sem er öfugt skástrik (\). Þar með erum við búin að segja Python túlknum að innri tvíhöggin séu bara hluti af strengnum.

```python
>>> "Jújú, þetta var bara \"fínt\""
'Jújú, þetta var bara "fínt"'
```

#### Strengir sem spanna margar línur

En hvað ef strengurinn minn þarf að hafa línuskil (vera í mörgum línum)? Ef við hefjum strenginn á þremur höggum `'''` (eða `"""`), þá getum við skrifa hverja línu fyrir sig, ýtt svo á færsluhnappinn (*enter* takkann). Við það förum við áfram niður um eina línu. Punktarnir þrír sem birtast `...` eru bara til að gefa til kynna að við séum enn að skrifa strenginn. Við getum byrjað á nýrri línu eins oft og við þurfum. Til að enda strenginn lokum við honum með `'''` og ýtum á færsluhnappinn:

```python
>>> '''Þetta er lína nr. 1
... þetta er lína nr. 2
... þetta er lína nr. 3
... þetta er lína nr. 4
... hvað þarftu margar línur?'''
'Þetta er lína nr. 1\nþetta er lína nr. 2\nþetta er lína nr. 3\nþetta er lína nr. 4\nhvað þarftu margar línur?'
>>>
```

*Hvers vegna var strengurinn prentaður í einni línu og með einhver öfug skástrik og n (*`\n`*) á milli?*  
Strengurinn var í raun birtur eins og hann er geymdur. Til að Python viti hvar við viljum hafa línuskil, þá bætir það `\n` inn í strenginn þar sem nýja línan á að byrja. Ef við notum prentfallið `print()`, þá les Python línuskilin og raðar línunum eins og við viljum:

```python
>>> print('''Þetta er lína nr. 1
... þetta er lína nr. 2
... þetta er lína nr. 3
... þetta er lína nr. 4
... hvað þarftu margar línur?''')
Þetta er lína nr. 1
þetta er lína nr. 2
þetta er lína nr. 3
þetta er lína nr. 4
hvað þarftu margar línur?
>>>
```

Við getum líka alltaf bætt `\n` inn í strengina okkar ef við viljum fá línuskil:

```python
>>> print("Stundum\ner flott að\nfara í nýja\nlínu\n\nen ekki alltaf ;)")
Stundum
er flott að
fara í nýja
línu

en ekki alltaf ;)
>>>
```


#### Lausnarstafur (e. Escape key)

Lausnarstafur er notaður við aðstæður þar sem við viljum breyta ákveðinni virkni. Við getum bæði notað hann til að koma í veg fyrir ákveðna virkni og til að draga fram ákveðna virkni.
Í Python er lausnarstafurinn það sem við köllum *öfugt skástrik* `\`.
Við notum hann til dæmis til að þvinga nýja línu í streng með því að nota `\n`.
```python
>>> nyr_strengur = 'Góðan daginn \nHvað segir þú gott?'
>>> print(nyr_strengur)
Góðan daginn
Hvað segir þú gott?
>>>
```
Við gætum þurft til dæmis þurft að nota lausnarstaf til að láta vita að við viljum ekki loka streng.
```python
>>> annar_strengur = 'Er betra að nota " eða \' fyrir strengi?'
>>> print(annar_strengur)
Er betra að nota " eða ' fyrir strengi?
>>>
```
Ef við hefðum ekki notað lausnarstafinn í seinna dæminu, þá hefðum við fengið villuboð því Python-túlkurinn hefði talið að seinni komman (') hefði verið til að loka strengnum. Allt sem á eftir henni kæmi í sömu línu væri því bara bull sem þyrfti að laga.
```python
>>> annar_strengur = 'Er betra að nota " eða ' fyrir strengi?'
  File "<stdin>", line 1
    annar_strengur = 'Er betra að nota " eða ' fyrir strengi?'
                                                   ^
SyntaxError: invalid syntax
```


#### Strengjum skeytt saman

Við getum skeytt saman mörgum strengjum með því að setja ```+``` á milli þeirra og þar með verða þeir að einum. Höfum þó í huga að ekki verður bætt við bili á milli þeirra.

```python
>>> "gaman" + "saman"
'gamansaman'
>>>
```

Við gætum bætt við bili, aftast í fyrri strenginn...

```python
>>> "gaman " + "saman"
'gaman saman'
>>>
```

Ef við notum hins vegar `print()` getum við prentað báða strengina og sett kommu (`,`) á milli þeirra. Þá sér prentfallið um að hafa bil á milli þeirra:

```python
>>> print("gaman","saman")
gaman saman
>>>
```

Við getum ekki skeytt strengjum við önnur gagnatög með `+` án þess að umbreyta þeim í streng fyrst.

```python
>>> "Svar við öllu: " + 42
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: Can't convert 'int' object to str implicitly
>>>
```

Við gætum skipt um tag fyrir heiltöluna 42

```python
>>> "Svar við öllu: " + str(42)
'Svar við öllu: 42'
```

Ef við notum `print()` og kommu (`,`) þurfum við ekki að hafa áhyggjur af þessu því `print()` sér um að breyta öðrum gagnatögum í streng.

```python
>>> print("Svar við öllu:", 42)
Svar við öllu: 42
```

#### Tagskipti - og strengurinn verður ...

Hver er munurinn á `1337` og `'1337'` ? Voðalega lítill en samt er þarna mikilvægur munur vegna þess að fyrra gildið er heiltala en hið síðara er strengur. Við vorum búin að komast að því að við getum ekki skeytt streng saman við önnur gagnatög án þess að umbreyta þeim fyrst í streng. En ef við umbreytum heiltölunni `1337` í streng og skeytum saman við strenginn `'1337'` hvað gerist þá? Við getum notað fallið `str()` sem breytir öðru tagi í streng

```python
>>> print(str(1337) + '1337')
13371337
>>>
```

Þetta var [umdeilanlega] áhugavert en ekki það sem við vorum að leita eftir. 1337 + 1337 er alveg örugglega ekki 13371337.  
Rétt eins og við getum breytt heiltölu í kommutölu og öfugt með föllunum `int()` og `float()`, þá getum við umbreytt streng í heiltölu eða kommutölu. Hins vegar verður strengurinn að vera tala. Strengurinn má ekki innihalda bókstafi eða önnur tákn og kallast tölustafastrengur (e. numeric string). Hann má reyndar innihalda punkt og þá getur hann orðið að kommutölu.

```python
>>> int(55a)
  File "<stdin>", line 1
    int(55a)
          ^
SyntaxError: invalid syntax
>>>
```
```python
>>> int('55')
55
>>>
>>> float('123.456')
123.456
>>>
```

Þá skulum við prófa aftur að leggja saman `1337` og `'1337'` en í þetta skiptið skulum við breyta strengnum í heiltölu:

```python
>>> print(1337 + int('1337'))
2674
>>>
```

#### Útdráttur (e. extraction) og Sneiðing (e. slicing)

Til að sækja ákveðinn hluta strengs getum við notað útdráttur (e. extraction). Það gerum við með því að setja hornklofa fyrir aftan breytunafn viðkomandi strengs. Skoðum dæmi þar sem við skilgreinum streng sem breytu og sækjum (drögum út úr) honum ákveðinn staf með því að vísa í sætisnúmerið hans (e. index).

```python
>>> skilabod = 'Þetta er góður dagur'
>>> print(skilabod[1])
e
```

Mögulega hefur einhver skoðað síðasta dæmið og orðið hissa þegar við prentuðum stak/staf númer `1` og fengum `e` sem er ekki fyrsti stafurinn í strengnum. Þarna komum við að nokkru sem mörgum finnst skrítið í fyrstu en flestir, ef ekki allir venjast mjög fljótt.
Þegar við notum vísa (e. index) með tögum sem innihalda hluti (strengir innihalda stafi), þá þurfum við að byrja að telja frá **núlli**.
Ef við skoðum aftur sama dæmið og síðast en vísum núna í sæti `0`, þá ættum við að fá fyrsta stafinn úr strengnum.

```python
>>> skilabod = 'Þetta er góður dagur'
>>> print(skilabod[0])
Þ
```

Nú þegar við vitum hvernig er hægt að sækja einstaka stafi úr strengjum, þá viljum við auðvitað vita hvernig við sækjum marga stafi í einu.
Við getum sótt hluta úr streng með aðferð sem kallast að *sneiða* (e. slicing) strengi. Við tilgreinum fyrsta og síðasta stafinn sem við viljum sækja með því að setja sætistölur þeirra innan hornklofa með tvípunkt á milli. Nú skulum við ná orðinu *góður* úr strengnum sem við notuðum síðast.

```python
>>> skilabod = 'Þetta er góður dagur'
>>> print(skilabod[9:12])
góð
```

Þarna settum við hornklofa aftan við breytuna `skilabod` og drógum út alla stafi frá sæti nr. 9 og **að** sæti nr. 12 (stafur nr. 12 fylgir **ekki** með). Athugið að stafurinn `ð` er númer 11.
Í sumum tilfellum viljum við sneiða frá öðrum hvorum enda strengsins og þá megum við sleppa sætistölunni þeim megin. Skoðum dæmi þar sem við draga út úr strengnum `Þetta er` en þá getum við sleppt tölunni, þó svo að það sé frekar einfalt að setja bara núll.

```python
>>> skilabod = 'Þetta er góður dagur'
>>> print(skilabod[:8])
Þetta er
>>> print(skilabod[0:8])
Þetta er
```

Til að sneiða aftan af strengnum sleppum við svo bara seinni tölunni. Kosturinn við það sá að þá þurfum við ekki að vita síðustu sætistöluna.

```python
>>> skilabod = 'Þetta er góður dagur'
>>> print(skilabod[15:])
dagur
```

**Stutt samantekt fyrir sneiðingu:**    
`[:]` dregur út alla rununa (allan strenginn), frá upphafi til enda.  
`[upphafstala :]` dregur út hlutann frá upphafstölunni (sem við skilgreindum) og að enda.
`[: endatala]` dregur út hluta frá upphafi (sæti 0) og að endatölunni (sem við skilgreindum).  
`[upphafstala : endatala]` dregur einungis út þann hluta sem er frá *upphafstölunni* og <u>að</u> *endatölunni*.  
`[upphafstala : endatala : skrefastærð]` dregur, eins og í dæminu á undan það bil sem við skilgreinum en hoppar yfir stök. Ef við setjum `2` í skrefastærð, þá drögum við út annað hvert stak.  

### Strengir - innbyggð föll

Python forritunarmálinu fylgja ýmis innbyggð föll og sum þeirra eru sérstaklega fyrir strengi. Þar sem að strengir eru óbreytanlegir (e. immutable), þá er gott að hafa í huga að þegar við notum þessi föll, þá erum við ekki að breyta upprunalega strengnum heldur býr Python til nýjan streng með þeim breytingum sem viðkomandi fall hefur í för með sér og skilar honum. Ef við viljum geyma útkomuna, þá höfum við síðan möguleika á að færa útkomuna undir nýja breytu eða endurnýta upprunalega breytunafnið.
<br>
Hér á eftir skoðum við nokkur algeng föll, hvað þau gera og hvernig við notum þau. Flest þeirra virka þannig að fallinu er skeytt aftan við breytu með punkt á milli (`breyta.fall()`). Í sumum tilfellum viljum við nota fleiri en eitt fall í einu og þá getum við skeytt fleiri föllum aftan á hvert annað (`breyta.fall_1().fall_2()`).

#### upper() og lower()

Fallið `upper()` hefur þá virkni að gera alla stafi í streng að hástöfum.

```python
>>> strengur = 'Eru ekki allir hressir?'
>>> print(strengur.lower())
eru ekki allir hressir?
```

Öfugt við `upper()` þá hefur `lower()` þá virkni að gera alla stafi að lágstöfum.

```python
>>> strengur = 'Eru ekki allir hressir?'
>>> print(strengur.upper())
ERU EKKI ALLIR HRESSIR?
```

#### captalize() og title()

Fallið `capitalize()` gerir <u>einungis fyrsta</u> staf strengs að hástaf.

```python
>>> nafn = 'svampur sveinsson'
>>> print(nafn.capitalize())
Svampur sveinsson
```

Fallið `title()` gerir ekki bara fyrsta staf strengsins að hástaf heldur gerir það <u>fyrsta staf allra orða</u> innan strengsins að hástaf.

```python
>>> nafn = 'svampur sveinsson'
>>> print(nafn.title())
Svampur Sveinsson
```

#### len()

Fallið `len()` hefur það hlutverk að segja okkur hver „lengd“ tiltekinnar breytu er. Þetta fall er ekki einungis ætlað strengjum en þegar við notum það með strengjum þá skilar það okkur fjölda stafa í streng og þá eru bil talin með. Skilagildi (e. return value) fallsins er alltaf heiltala.
Öfugt við flest föll sem við skoðum í þessum hluta, þá er þessu falli ekki skeytt aftan á breytuna í kóðanum heldur setjum við breytunafnið á milli sviga fallsins (`fall(breyta)`).

```python
>>> langur_strengur = 'Ekkert svo langur!'
>>> len(langur_strengur)
18

```

#### index()

Fallið `index()` skilar sætistölu ákveðins lesgildis (stafs) í streng. Við tilgreinum það lesgildi sem við viljum fá innan sviga fallsins. Eins og fram kom í kaflanum um útdrátt og sneiðingu, þá er fyrsta sætistalan núll.   
Við þurfum svo að muna að hafa högg utan um lesgildi (staf) sem við leitum af.

```python
>>> stafir = 'abcdefg'
>>> stafir.index('d')
3
```

#### join()

Fallið `join()` skeytir saman tveimur strengjum. Til að nota það skeytum við því bæði aftan við breytu (eða streng sem er ekki innan breytu) og það tekur við breytu innan sviganna (`strengur1.join(strengur2)`). Það sem gerist síðan er að fallið tekur fyrri strenginn og stingur honum inn á milli allra stafa seinni strengsins.

```python
>>> a = 'abc'
>>> b = '123'
>>> print(a.join(b))
1abc2abc3
>>> print(b.join(a))
a123b123c
```

Skoðum annað dæmi þar sem við notum fallið til að bæta bilum inn í streng. Takið eftir að *bilið* er á milli högganna (```' '```).

```python
>>> hetja = 'Batman'
>>> print(' '.join(hetja))
B a t m a n
```

Fallið `join()` er einnig notað fyrir lista og verður fjallað nánar um það í kaflanum um *föll fyrir lista*.

#### split()

Fallið `split()` tekur streng og færir inn í lista. Ef strengurinn inniheldur fleiri en eitt orð (og þar með bil), þá færir hann hvert orð sem einn streng inn í listann. Fallið virkar líka þó svo að strengurinn innihaldi bara eitt orð og þá verður til listi sem inniheldur einn streng. Sé strengurinn tómur (inniheldur ekki staflesgildi (e.literal)) verður til tómur listi.

```python
>>> setning = 'Fyrr var oft í koti kátt'
>>> setning.split()
['Fyrr', 'var', 'oft', 'í', 'koti', 'kátt']
```

#### input()

Fallið `input()` er notað til að búa til streng úr ílagi (e. input) frá notanda. Þetta ílag er fengið á meðan forritið keyrir og fært undir breytu. Við höfum möguleika á að láta kvaðningu fylgja, það er að segja texta sem notandinn sér og inniheldur gjarnan einhverjar leiðbeiningar. Þessa kvaðningu setjum við sem streng á milli sviga fallsins (`breyta = input('Einhver texti...')`)

```python
>>> nafn = input('Sláðu inn nafn: ')
Sláðu inn nafn: Sverrir
>>> print('Halló', nafn)
Halló Sverrir
```

#### format()

Fallið `format()` gerir okkur kleift að skipta út frátektarstað (e. placeholder) í streng fyrir breytu. Með þessu getum við til dæmis endurnýtt strengi. Frátektarstaðurinn er táknaður með tómu slaufusvigapari ```{}```. Fallinu er skeytt aftan við streng með punkt á milli og innan sviga fallsins tilgreinum við þær breytur sem verða notaðar. Ef frátektarstaðirnir í strengnum eru fleiri en einn, þá þurfa breyturnar innan sviganna að vera í sömu röð.

```python
>>> fjoldi_stada = 99
>>> print('Skilaboð voru send á {} staði'.format(einhver_tala))
Skilaboð voru send á 99 staði

>>> fjoldi_skilaboda = 5
>>> print('{} skilaboð voru send á {} staði'.format(fjoldi_skilaboda, einhver_tala))
5 skilaboð voru send á 99 staði
```

#### replace()

Fallið `replace()` er notað til að skipta út hluta úr streng og bætir nýjum s. Fallinu er skeytt aftan við streng með punkt á milli. Innan sviganna þarf annars vegar að tilgreina þann hluta sem skal fjarlægður, hins vegar þann streng (getur verið breyta) sem koma skal í staðinn og þeir aðskildir með kommu.

```python
>>> strengur = '"Aggagagg" sagði tófa á grjóti'
>>> nyr_strengur = strengur.replace('Aggagagg', 'Jibbí jei')
>>> print(nyr_strengur)
"Jibbí jei" sagði tófa á grjóti

>>> texti = 'Góðan daginn'
>>> annar_strengur = strengur.replace('Aggagagg', texti)
>>> print(annar_strengur)
"Góðan daginn" sagði tófa á grjóti
```

#### startswith() og endswith()

Fallið `startswith()` kannar hvort að strengur byrji á ákveðnum stöfum. Skilagildið er Boole-tag (e. boolean), annað hvort ```True``` eða ```False```. Fallinu er skeytt aftan við streng með punkt á milli og gildi (innan sviga) fallsins er sá strengur sem við viljum sannreyna. Mikilvægt er að hafa í huga að fallið er hástafanæmt (e. case sensitive).  
Fallið `endswith()` kannar hins vegar hvort að strengur *endi* á ákveðnum stöfum en virkar að öðru leyti alveg eins og `startswith()`.  

```python
>>> strengur = 'From cfarley@iupui.edu Fri Jan  4 14:50:18 2018'
>>> strengur.startswith('From')
True
>>> strengur.startswith('nammi')
False
```

Mikilvægt er að hafa í huga að þegar við leitum eftir tölum í strengjum, þá verðum við að leita með streng vegna þess að þessi föll taka einungis inn strengi. Ef við reynum að kalla á fallið með einhverju öðru gildi en streng fáum við villuboð.

```python
>>> strengur.endswith('2018') # Hér er 2018 strengur
True
>>> strengur.endswith(2018) # Hér er 2018 heiltala
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: endswith first arg must be str or a tuple of str, not int
```

#### count()
Til að telja hversu oft eitthvað tiltekið gildi kemur fyrir í streng getum við notað fallið `count()`. Fallinu er skeytt aftan við streng með punkt á milli. Þegar kallað er á fallið þarf að tilgreina það gildi (streng) sem skal talið. Strengurinn sem fallið tekur inn (leitarstrengurinn) getur verið staflesgildi að lengd eða lengri. Mikilvægt er að hafa í huga að fallið er hástafanæmt (e. case sensitive). Skilagildi fallsins er heiltala og ef ekkert tilvik finnst, þá verður skilagildið núll (```0```).

```python
>>> strengur = 'tommi.tomatur@toto.co.uk'
>>> strengur.count('.')
3
>>> strengur.count('to')
4
```

#### isalnum() og isalpha()

Fallið `isalnum()` segir okkur hvort að strengur innihaldi einungis tölustafi. Fallið skilar Boole-gildi, ```True``` ef strengur innhildur einungis tölustafi, annars ```False```. Fallinu er skeytt aftan við streng með punkt á milli en tekur ekkert gildi inn þegar kallað er á það (tómir svigar = ```strengur.isalnum()```). Mikilvægt er að hafa í huga að isalnum() skilar ```False``` ef strengur inniheldur kommutölu og því getur verið betra að nota fallið `isdigit()` ef markmiðið er að kanna hvort strengur sé annað hvort kommutala eða heiltala.

```python
>>> strengur_heil = '12345'
>>> strengur_heil.isalnum()
True
>>> strengur_kommu = '123.456'
>>> strengur_kommu.isalnum()
False
```

Fallið `isalpha()` kannar hvort að strengur innihaldi einungis bókstafi, enga tölustafi eða tákn eins og til dæmis punkta eða spurningamerki. Fallinu er skeytt aftan við streng með punkt á milli en tekur ekkert gildi inn þegar kallað er á það (tómir svigar = ```strengur.isalpha()```). Fallið skilar Boole-gildi, ```True``` ef strengur innhildur einungis bókstafi, annars ```False```.

```python
>>> gata_nafn = 'Miklabraut'
>>> gata_numer = '99'
>>> gata_nafn.isalpha()
True
>>> gata_numer.isalpha()
False
```

#### strip()
Fallið `strip()` fjarlægir autt pláss (bil) fremst og aftast í streng ef eitthvað er. Skilagildi fallsins er sami strengur þar sem bil fremst og aftast hafa verið fjarlægð ef einhver voru.

```python
>>> strengur_bil = '   Halló heimur!   '
>>> strengur_ekki_bil = 'Góðan daginn'
>>> strengur_bil.strip()
'Halló heimur!'
>>> strengur_ekki_bil.strip()
'Góðan daginn'
```
